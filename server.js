/* eslint-env node, es6 */
const express = require('express');

// bot framework for interacting with the wiki, see https://www.npmjs.com/package/mwn
// moved to route files
// const { mwn } = require('mwn');
// const { default: title } = require('mwn/build/title');

// sql client for database accesses, see https://www.npmjs.com/package/mysql2
// const mysql = require('mysql2/promise');

const app = express();

app.use(express.urlencoded({ extended: false })); // for parsing the body of POST requests

app.use(express.static(__dirname + 'static')); // serve files in the static directory

const port = parseInt(process.env.PORT, 10); // necessary for the tool to be discovered by the nginx proxy

// You may want to sign in with a bot account if you want to make use of high bot
// API limits, otherwise just remove the username and password fields below.

/* async function getDbConnection() {
        return await mysql.createConnection({
                host: 'enwiki.analytics.db.svc.eqiad.wmflabs',
                port: 3306,
                user: crendentials.db_user,
                password: crendentials.db_password,
                database: 'enwiki_p'
        });
}*/

// Serve index.html as the homepage
const indexRoute = require('./static/index.js');
app.use('/', indexRoute);
        //res.sendFile(__dirname + '/static/index.html');

app.get('/favicon.ico', (req, res) => {
        res.sendFile(__dirname + '/static/favicon.png');
});

app.get('/static/patrol.js', (req, res) => {
        res.sendFile(__dirname + '/static/patrol.js');
});

app.get('/static/sorttable.json', (req, res) => {
	res.sendFile(__dirname + '/static/sorttable.js');
});

app.get('/static/strings.json', (req, res) => {
        res.sendFile(__dirname + '/static/strings.json');
});

app.get('/static/styles.css', (req, res) => {
        res.sendFile(__dirname + '/static/styles.css');
});

// *** dynamic routes *** //
// patrol
app.get('/patrol', (req, res) => {
        res.redirect('/patrol/hr');
});

app.use('/patrol', require('./routes/patrol.js'));

// glas
app.use('/glas', require('./routes/glas.js'));

// nacrt
app.use('/nacrt', require('./routes/nacrt.js'));

// popis
//app.use('/popis', require('./routes/popis.js'));

// jubilarac
app.use('/jubilarac', require('./routes/jubilarac.js'));

// predlozak2 - pretraga po svim parametrima
app.use('/predlozak2', require('./routes/predlozak2.js'));
app.get('/predlozak', (req, res) => {
	res.redirect('/predlozak2/hr');
});
app.get('/predlozak2', (req, res) => {
	res.redirect('/predlozak2/hr');
});

// typo - pretraga pravopisnih gresaka
app.use('/typos', require('./routes/typos.js'));
app.get('/typos', (req, res) => {
	res.redirect('/typos/hr');
});
app.use('/hunspell', require('./routes/hunspell.js'));

app.use('/url', require('./routes/url.js'));

app.get('/url', (req, res) => {
	res.redirect('/url/hr');
});

app.use('/ratovi', require('./routes/ratovi.js'));
app.use('/kategorija', require('./routes/kategorija.js'));
app.use('/rob', require('./routes/rob.js'));
app.use('/najcitaniji', require('./routes/najcitaniji.js'));

app.use('/catcheck', require('./routes/catcheck.js'));

// refresh wp/rob automatically
const cron = require('node-cron')
const axios = require('axios')
// every 6 hours
cron.schedule('0 */6 * * *', async () => {
    try {
        console.log('cron: /rob');
        const response = await axios.get(`http://localhost:${port}/rob`);
    } catch (error) {
        console.error('cron - ERROR on /rob:', error.message);
    }
});

//6:15 on every 2nd of each month
cron.schedule('15 6 2 * *', async () => {
    try {
        console.log('cron: /najcitaniji');
        const response = await axios.get(`http://localhost:${port}/najcitaniji`);
    } catch (error) {
        console.error('cron - ERROR on /najcitaniji:', error.message);
    }
});

app.listen(port, () => console.log(`Example app listening at port ${port}`));

const generateHead = function generateHead(title, error = 0, sortTable = 0) {
	var retVal = '<html lang="hr"><head>'
		+ '<meta charset="UTF-8">'
		+ '<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">';
	retVal = retVal + '<title>' + title + (error ? '[+]' : '') + ' </title>';
	if (sortTable) {
		retVal = retVal + '<script src="/static/sorttable.json"></script>';
	}
	let bootstrap = `
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
		<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>
	`
	retVal = retVal + bootstrap + '<link rel="stylesheet" type="text/css" href="/static/styles.css">';
	retVal = retVal + '</head>';
	return retVal;
}

const generateErrorPage = function generateErrorPage(title, message) {
	var retVal = generateHead(title, 1);
	retVal = retVal + '<body>' + nav + '<div class="container" style="padding-top: 10px;">'
		+ colorText('red', message, 1)
		+ '</div></body>';
	return retVal;
}

const getTranslation = function getTranslation(lang, string, v1, v2, v3, v4, v5) {
	const transLangs = ['bs', 'hr', 'sh', 'en'];
	if (!transLangs.includes(lang)) {
		lang = 'en';
	}

	const strings = require('../static/strings.json');
	let text = '';

	if (strings[lang]) {
		text = strings[lang][string];
	} else {
		text = strings.hr[string];
	}

	if (v1) {
		let stringHelper = text.toString()
			.replace(/\$1/g, v1)
			.replace(/\$2/g, v2)
			.replace(/\$3/g, v3)
			.replace(/\$4/g, v4)
			.replace(/\$5/g, v5);

		// Extracting quantity strings
		const quantityStringMatches = stringHelper.match(/{([^}]+)}/g);
		if (quantityStringMatches) {
			quantityStringMatches.forEach(quantityStringMatch => {
				const quantityValues = quantityStringMatch.substring(1, quantityStringMatch.length - 1).split(',');
				const number = parseInt(quantityValues[0].trim());
				let chosenString = '';

				// Choosing appropriate string format
				if (number % 10 === 1 && number % 100 !== 11) {
					chosenString = quantityValues[1].trim();
				} else if (
					number % 10 >= 2 &&
					number % 10 <= 4 &&
					(number % 100 < 12 || number % 100 > 14)
				) {
					chosenString = quantityValues[2].trim();
				} else {
					chosenString = quantityValues[3].trim();
				}

				// Include the number value and chosen quantity string in the final output
				stringHelper = stringHelper.replace(quantityStringMatch, `${number} ${chosenString}`);
			});
		}

		return stringHelper;
	} else {
		return text;
	}
}




const failureCallback = function failureCallback(err, req, res) {
	res.write('<br><span class="errorMsg">' + getTranslation('en', 'errMsg') + '</span>')
	res.write('<div class="errorTrace">');
	const greska = processError(err);
	res.write(greska);
	res.write('</div>');
	res.write('<script>document.title+=" [+]"</script>')
}

const processError = function processError(err) {
	const greska = JSON.stringify(err.stack)
		.replace(/\\\\/g, '/')
		.replace(/\\n/g, '<br>');
	console.log(greska);
	return greska;
}

const colorText = function colorText(color, text, div = 0, background = 0) {
	let bg = (background) ? 'background-' : '';
	if (div == 1 || div == 'div') {
		return '<div style="' + bg + 'color: ' + color + '; font-weight: bold;">' + text + '</div>';
	} else {
		return '<span style="' + bg + 'color: ' + color + '; font-weight: bold;">' + text + '</span>';
	}
}

/**
	 * Chooses the plural form of a string in Croatian
	 * "@" serves as a special character for number insertion
	 * @param {number} br - number
	 * @param {string} jedan - "one" form, 1
	 * @param {string} dva - "few" form, 2-4
	 * @param {string} pet - "many" form
	 */
const mnozina = function mnozina(br, jedan, dva, pet) {
	const finalOne = jedan.includes('@') ? jedan.replace('@', br) : `${br} ${jedan}`;
	const finalFew = dva.includes('@') ? dva.replace('@', br) : `${br} ${dva}`;
	const finalMany = pet.includes('@') ? pet.replace('@', br) : `${br} ${pet}`;

	if (br % 10 == 1 & br % 100 != 11) {
		return finalOne;
	} else if (br % 10 >= 2 && br % 10 <= 4 && (br % 100 < 12 || br % 100 > 14)) {
		return finalFew;
	} else {
		return finalMany;
	}
}

const escapeSpecials = function escapeSpecials(str) {
	let newStr = str
		.replaceAll('&', "&amp;")
		.replaceAll('"', "&quot;")
		.replaceAll('\'', "&apos;")
		.replaceAll('<', "&lt;")
		.replaceAll('>', "&gt;")
		.trim();
	return newStr;
}

const nav = `
<style>
a.nav-link {
    color: white !important;
}
</style>

<nav class="navbar navbar-expand-lg sticky-top navbar-dark" style="background-color: #0062cc;">
  <div class="container">
    <a class="navbar-brand" href="/">Ivi's Tools</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="/">Home</a>
        </li>
		<!--
        <li class="nav-item">
          <a class="nav-link" href="#">Features</a>
        </li>
		-->
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Patrol
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="/patrol/hr">hrwiki</a>
            <a class="dropdown-item" href="/patrol/sh">shwiki</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/patrol/ar">arwiki</a>
            <a class="dropdown-item" href="/patrol/de">dewiki</a>
            <a class="dropdown-item" href="/patrol/es">eswiki</a>
			<a class="dropdown-item" href="/patrol/nl">nlwiki</a>
			<a class="dropdown-item" href="/patrol/sv">svwiki</a>
            <a class="dropdown-item" href="/patrol/wikidata">wikidata</a>
          </div>
        </li>

		<li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Alati
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="/glas">Pravo glasa</a>
            <a class="dropdown-item" href="/jubilarac">Popis jubilarnih članaka</a>
            <a class="dropdown-item" href="/nacrt">Nacrt</a>
            <a class="dropdown-item" href="/ratovi">Ratovi izmjenama</a>
            <a class="dropdown-item" href="/kategorija">Kategorija</a>
			<a class="dropdown-item" href="/hunspell">Provjera pravopisa nedavnih izmjena</a>
          </div>
        </li>

		<li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Multi-language
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="/predlozak2">Template parameter usage</a>
            <a class="dropdown-item" href="/typos">Check recent edits for typos</a>
            <a class="dropdown-item" href="/url">Articles using shortened URLs</a>
            <a class="dropdown-item" href="/catcheck">Compare category members to another language</a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>

`;

const userAgent = "hrwp.toolforge.org (via [[User:IviBot]])";

const wikiLangs = ["ab", "ace", "ady", "af", "als", "alt", "am", "ami", "an", "ang", "anp", "ar", "arc", "ary", "arz", "as", "ast", "atj", "av", "avk", "awa", "ay", "az", "azb",
	"ba", "ban", "bar", "bat-smg", "bbc", "bcl", "be", "be-tarask", "bg", "bh", "bi", "bjn", "blk", "bm", "bn", "bo", "bpy", "br", "bs", "bug", "bxr",
	"ca", "cbk-zam", "cdo", "ce", "ceb", "ch", "chr", "chy", "ckb", "co", "cr", "crh", "cs", "csb", "cu", "cv", "cy",
	"da", "dag", "de", "dga", "din", "diq", "dsb", "dty", "dv", "dz",
	"ee", "el", "eml", "en", "eo", "es", "et", "eu", "ext",
	"fa", "fat", "ff", "fi", "fiu-vro", "fj", "fo", "fon", "fr", "frp", "frr", "fur", "fy",
	"ga", "gag", "gan", "gcr", "gd", "gl", "glk", "gn", "gom", "gor", "got", "gpe", "gu", "guc", "gur", "guw", "gv",
	"ha", "hak", "haw", "he", "hi", "hif", "hr", "hsb", "ht", "hu", "hy", "hyw",
	"ia", "id", "ie", "ig", "ik", "ilo", "inh", "io", "is", "it", "iu",
	"ja", "jam", "jbo", "jv",
	"ka", "kaa", "kab", "kbd", "kbp", "kcg", "kg", "ki", "kk", "kl", "km", "kn", "ko", "koi", "krc", "ks", "ksh", "ku", "kv", "kw", "ky",
	"la", "lad", "lb", "lbe", "lez", "lfn", "lg", "li", "lij", "lld", "lmo", "ln", "lo", "lt", "ltg", "lv",
	"mad", "mai", "map-bms", "mdf", "mg", "mhr", "mi", "min", "mk", "ml", "mn", "mni", "mnw", "mr", "mrj", "ms", "mt", "mwl", "my", "myv", "mzn",
	"nah", "nap", "nds", "nds-nl", "ne", "new", "nia", "nl", "nn", "no", "nov", "nqo", "nrm", "nso", "nv", "ny",
	"oc", "olo", "om", "or", "os",
	"pa", "pag", "pam", "pap", "pcd", "pcm", "pdc", "pfl", "pi", "pih", "pl", "pms", "pnb", "pnt", "ps", "pt", "pwn",
	"qu",
	"rm", "rmy", "rn", "ro", "roa-rup", "roa-tara", "ru", "rue", "rw",
	"sa", "sah", "sat", "sc", "scn", "sco", "sd", "se", "sg", "sh", "shi", "shn", "si", "simple", "sk", "skr", "sl", "sm", "smn", "sn", "so", "sq", "sr", "srn", "ss", "st", "stq", "su", "sv", "sw", "szl", "szy",
	"ta", "tay", "tcy", "te", "tet", "tg", "th", "ti", "tk", "tl", "tly", "tn", "to", "tpi", "tr", "trv", "ts", "tt", "tum", "tw", "ty", "tyv",
	"udm", "ug", "uk", "ur", "uz",
	"ve", "vec", "vep", "vi", "vls", "vo",
	"wa", "war", "wo", "wuu",
	"xal", "xh", "xmf",
	"yi", "yo",
	"za", "zea", "zgh", "zh", "zh-classical", "zh-min-nan", "zh-yue", "zu"]

const chooser = function (res, url) {
	res.write('<br /><hr /><br />Choose project: <div style="text-align: center;">');
	let count = 0;
	wikiLangs.forEach(e => {
		if (count % 30 == 0) res.write('<br />');
		res.write('<a href="/' + url + '/' + e + '">' + e + '</a>');
		if (count < wikiLangs.length - 1) res.write(' - ');
		count++;
	});

	res.write('</div>'); // center
}




module.exports = {
	generateHead, generateErrorPage, nav, colorText, mnozina, escapeSpecials, failureCallback, processError, getTranslation, userAgent, wikiLangs,
	chooser
}
const express = require('express')

const { mwn } = require('mwn');
const { default: title } = require('mwn/build/title');

var init = require('./init.js');

const router = express.Router()

router.get("/", (req, res) => {

	res.write(init.generateHead('Ivi\'s Tools'));

	res.write(init.nav);

	a = `<div class="container">
	<div style="text-align: center; font-size: 24px;">Welcome!</div>
	<div>Patrol status of supported projects:</div>
	<ul>
		<li><a href="/patrol/hr">hrwiki</a></li>
		<li><a href="/patrol/sh">shwiki</a></li>
		<li><a href="/patrol/wikidata">wikidata</a></li>
	</ul>
	<h1>List of tools for Croatian Wikipedia</h1>
	<ul>
		<li><a href="/glas">Provjeri pravo glasa</a></li>
		<li><a href="/jubilarac">Popis jubilarnih članaka po datumu</a></li>
		<li><a href="/nacrt">Popis nacrta koji ne sadrže Predložak:Nacrt</a></li>
		<li><a href="/ratovi">Popis članaka s višestrukim nedavnim izmjenama i vraćanjima</a></li>
		<li><a href="/kategorija">Provjerava je li unesena kategorija pretrpana (popisuje članke istovremeno prisutne u
				glavnoj kategoriji i njenim podkategorijama)</a></li>
		<li><a href="/najcitaniji">Ažuriraj predložak najčitanijih članaka u prošlom mjesecu</a></li>
		<li><a href="/rob">Ažuriraj popis i status WP:ROB u porukama sučelja</a></li>
		<li><a href="/hunspell">Provjeri pravopis nedavnih izmjena putem Hunspell rječnika</a></li>
	</ul>
	<h1>List of tools for all language projects</h1>
	<ul>
		<li><a href="/predlozak2">For a given template, list articles in which each parameter is or isn't used</a></li>
		<li><a href="/typos">Check recently edited articles for typos against 'WP:AutoWikiBrowser/Typos'</a></li>
		<li><a href="/url">List articles using shortened external links</a></li>
		<li><a href="/catcheck">List all articles in a category that exist in a different language</a></li>
	</ul>
</div>`

	res.write(a);
	res.end();

});

module.exports = router;





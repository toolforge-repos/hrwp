const express = require('express')

const { mwn } = require('mwn');
const { default: title } = require('mwn/build/title');

var init = require('../static/init.js');

const router = express.Router()

router.get("/", (req, res) => {

	if (req.query.username && req.query.date) {
		console.log('query pass');

		var username = req.query.username;
		var date = req.query.date;

		// ako nijedno nije null
		if (!username.match(regexp = "^[A-ž0-9,._ ]+$")) {
			console.log('/glas - name fail:' + username);
			// krivo ime
			res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
			res.write(init.generateErrorPage('Provjera prava glasovanja', 'Ime sadrži nedopuštene znakove.'));
			res.end();
			return
		}

		var dateParse = new Date(date);
		if (date.match(regexp = "^20(0[5-9]|1[0-9]|2[0-4])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$") && dateParse instanceof Date && !isNaN(dateParse)) {
			generateVoteCheckPage(req, res, username, dateParse);
		} else {
			console.log('/glas - date fail: ', date);
			// krivo ime
			res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
			res.write(init.generateErrorPage('Provjera prava glasovanja', 'Datum nije ispravan.'));
			res.end();
			return
		}
	} else {

		// username i date su null
		// forma
		res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
		res.write(init.generateHead('Provjera prava glasovanja'));

		res.write(init.nav);

		res.write('<div class="container" style="padding-top: 10px;">')
		res.write('Provjera prava glasa na Wikipediji na hrvatskome jeziku:<br />');

		var form = `<form action="/glas" method="get">

        <label for="username">Suradničko ime:</label>
        <input type="text" id="username" name="username" />
        <br />
        <label for="date">Datum početka glasovanja:</label>
        <select id="date" name="date">`;
		res.write(form);

		form = '';

		for (var i = 0; i < 10; i++) {
			var date = new Date();
			date.setDate(date.getDate() - i);
			form = form + '<option value="' + date.toISOString().split('T')[0] + '">' + date.toLocaleDateString('hr-HR') + '</option>';
		}

		form = form + `</select><br>
        <input type="submit" value="Provjeri pravo glasa"></input>
        </form>`;
		res.write(form);

		res.end();
	}
});


async function generateVoteCheckPage(req, res, username, dateParse) {
	apiUrl = 'https://hr.wikipedia.org';

	// bot account and database access credentials, if needed
	const crendentials = require('../../credentials.json');

	const strings = require("../static/strings.json");

	const client = await mwn.init({
		apiUrl: apiUrl + '/w/api.php',
		// Can be skipped if the bot doesn't need to sign in    
		username: crendentials.bot_username,
		password: crendentials.bot_password,

		// Set your user agent (required for WMF wikis, see https://meta.wikimedia.org/wiki/User-Agent_policy):    
		userAgent: init.userAgent,
		// Set default parameters to be sent to be included in every API request    
		defaultParams: {
			assert: 'user' // ensure we're logged in    
		}
	});

	// need to do either a .getSiteInfo() or .login() before we can use the client object  
	await client.getSiteInfo();
	//await client.login();

	console.log('/glas: ' + username + '; ' + dateParse);

	res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
	res.write(init.generateHead('Provjera prava glasovanja'));

	res.write(init.nav);

	res.write('<div class="container" style="padding-top: 10px;">')
	res.write('Provjera prava glasa za <b>' + username + '</b> - ');

	//provjera postoji li suradnik

	await client.request({
		"action": "query",
		"format": "json",
		"list": "users|usercontribs",
		"usprop": "editcount|gender|registration|groups|blockinfo",
		"ususers": username,
		"uclimit": "1",
		"ucuser": username,
		"ucdir": "newer",
		"ucprop": "timestamp"
	}).then(async function (data) {
		if (data.query.users[0].missing) {
			res.write(init.colorText("red", 'Suradnik "' + username + '" ne postoji!', 1));
		} else {
			pronoun = (data.query.users[0].gender == 'female' ? 'Suradnica' : 'Suradnik');
			res.write(init.colorText('green', pronoun + ' postoji...<br><br>'));

			//provjeri blokiran?

			if (data.query.users[0].blockid) {
				var datumIsteka = new Date(data.query.users[0].blockexpiry);
				var sedamDana = new Date();
				sedamDana.setUTCDate(sedamDana.getDay() - 7);
				var blockedPronoun = (data.query.users[0].gender == 'female' ? ' blokirana' : ' blokiran');

				res.write(init.colorText('red', pronoun + ' je ' + blockedPronoun + ".", 1));

				if (+datumIsteka < +sedamDana) {
					res.write(init.colorText('orange', 'Blok završava za manje od tjedan dana, molim ručno provjerite '
						+ 'hoće li blok završiti prije zaključivanja glasovanja.'));
				}
			}

			//provjeri ima li suradnik ijednu izmjenu
			if (data.query.users[0].editcount > 0) {
				var prviEdit = String(data.query.usercontribs[0].timestamp).split("T");

				//provjera prvog reda - date
				var danas = new Date();
				var dvaMjeseca = new Date();
				dvaMjeseca.setMonth(dvaMjeseca.getMonth() - 2);
				var prviEditDate = Date.parse(prviEdit[0]);

				// provjera više od dva mjeseca
				if (+dvaMjeseca > +prviEditDate) {

					// više od 200 izmjena
					if (parseInt(data.query.users[0].editcount) > 200) {

						res.write('<div>' + pronoun + ' ukupno ima ' + init.colorText('green', data.query.users[0].editcount
							+ ' izmjena') + ', prvu na datum ' + init.colorText('green', prviEdit[0]
								+ ' u ' + prviEdit[1].replace('Z', '')) + '.</div>');
					} else {
						res.write(init.colorText('red', pronoun + ' nema dovoljno ukupnih izmjena ('
							+ data.query.users[0].editcount + ' < 200)!', 'div'));
					}
				}
				else {
					// provjera više od dva mjeseca - fail
					res.write(init.colorText('red', pronoun + ' nema dva mjeseca staža!', 'div'));
				}

				//provjera mjesesc dana - za pravo glasanja na admin izborima
				var jedanMjesec = new Date();
				jedanMjesec.setMonth(jedanMjesec.getMonth() - 1);
				if (+jedanMjesec > +prviEditDate) {
					res.write(pronoun + init.colorText('green', ' ima dovoljan staž') + ' (mjesec dana) kako bi glasao na izboru administratora.');
				} else {
					res.write(pronoun + init.colorText('red', ' nema dovoljan staž') + ' (mjesec dana) kako bi glasao na izboru administratora.');
				}

			} else {
				res.write(init.colorText('red', pronoun + ' nema nijednu izmjenu!', 'div'));
				return;
			}

			// drugi poziv: izmjene u GIP - all time

			await client.request({
				"action": "query",
				"format": "json",
				"list": "usercontribs",
				"uclimit": "max",
				"ucuser": username,
				"ucprop": "",
				"ucnamespace": "0"
			}).then((data) => {
				res.write('<div>')
				if (data.query.usercontribs.length < 200) {
					res.write(init.colorText('red', pronoun + ' nema dovoljno izmjena u GIP-u (' + data.query.usercontribs.length + ' < 200)!'))
				}
				else {
					res.write(pronoun + ' ukupno ima ' + init.colorText('green', (data.query.usercontribs.length >= data.limits.usercontribs ?
						data.limits.usercontribs + ' ili više ' : data.query.usercontribs.length) + ' izmjena') + ' u GIP-u.');
				}
				res.write('</div>');
			})

			// treci poziv - izmjene i zadnjih godinu dana - all NS

			//var oneYearAgo = dateParse;
			var oneYearAgo = new Date(dateParse);

			oneYearAgo.setMonth(dateParse.getMonth() - 12);

			await client.request({
				"action": "query",
				"format": "json",
				"list": "usercontribs",
				"uclimit": "max",
				"ucstart": dateParse.toISOString(),
				"ucend": oneYearAgo.toISOString(),
				"ucuser": username,
				"ucprop": ""
			}).then((data) => {
				res.write('<br>');
				res.write('<div>U zadnjih godinu dana (od <b>' + oneYearAgo.toLocaleDateString('hr-HR') + '</b> do <b>' + dateParse.toLocaleDateString('hr-HR') + '</b>), ' + String(pronoun).toLowerCase()
					+ ' ukupno ima <b>'
					+ (data.query.usercontribs.length >= data.limits.usercontribs
						? data.limits.usercontribs + ' ili više ' : data.query.usercontribs.length)
					+ ' izmjena</b> u svim imenskim prostorima.</div>');

				if (data.query.usercontribs.length < 50) {
					res.write(init.colorText('red', pronoun + ' nema dovoljno ukupnih izmjena u zadnjih godinu dana!'));
				} else {
					res.write(init.colorText('green', pronoun + ' ima dovoljno ukupnih izmjena u zadnjih godinu dana!'));
				}
			})

			// cetvrti poziv - izmjene u zadnjih godinu dana - samo GIP

			await client.request({
				"action": "query",
				"format": "json",
				"list": "usercontribs",
				"uclimit": "max",
				"ucstart": dateParse.toISOString(),
				"ucend": oneYearAgo.toISOString(),
				"ucuser": username,
				"ucprop": "",
				"ucnamespace": "0"
			}).then((data) => {
				res.write('<br /><br />')
				res.write('<div>U istom periodu u GIP-u je <b>'
					+ (data.query.usercontribs.length >= data.limits.usercontribs
						? data.limits.usercontribs + ' ili više ' : data.query.usercontribs.length) + ' izmjena</b>.</div>');

				if (data.query.usercontribs.length < 50) {
					res.write(init.colorText('red', pronoun + ' nema dovoljno izmjena u GIP-u u zadnjih godinu dana!'));
				} else {
					res.write(init.colorText('green', pronoun + ' ima dovoljno izmjena u GIP-u u zadnjih godinu dana!'));
				}
			})

		}   // if user missing, else branch done
	})
		.finally(() => {
			res.write('<div style="padding-top: 20px;">Provjera završena!</div></body>');
			res.end();
		}).catch(err => { init.failureCallback(err, req, res); });
}

module.exports = router;
const express = require('express')

const { mwn } = require('mwn');
const { default: title } = require('mwn/build/title');

var init = require('../static/init.js');
const { DateTime } = require('luxon');

const router = express.Router()

const naslov = "Ažuriranje popisa najčitanijih članaka";


router.get("/", async (req, res) => {

	try {
		await generateResultPage(req, res);
	}
	catch (err) {
		init.failureCallback(err, req, res);
	}
	res.end()
});


async function generateResultPage(req, res, lang = "hr") {
	const startMem = process.memoryUsage().rss / 1024 / 1024; //MB
	const startTime = process.hrtime();

	apiUrl = 'https://' + lang + '.wikipedia.org';

	// bot account and database access credentials, if needed
	const crendentials = require('../../credentials.json');

	const client = await mwn.init({
		apiUrl: apiUrl + '/w/api.php',
		// Can be skipped if the bot doesn't need to sign in    
		username: crendentials.bot_username,
		password: crendentials.bot_password,

		// Set your user agent (required for WMF wikis, see https://meta.wikimedia.org/wiki/User-Agent_policy):    
		userAgent: init.userAgent,
		// Set default parameters to be sent to be included in every API request    
		defaultParams: {
			assert: 'user' // ensure we're logged in    
		}
	});

	// need to do either a .getSiteInfo() or .login() before we can use the client object  
	await client.getSiteInfo();
	//await client.login();

	console.log('/najcitaniji');

	res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
	res.write(init.generateHead(naslov));

	res.write(init.nav);

	res.write('<div class="container" style="padding-top: 10px;">');
	res.write("Ažuriranje teksta za <a href='https://hr.wikipedia.org/wiki/Predložak:Najčitaniji'>popis najčitanijih članaka</a><br>");
	const { year, month } = getPreviousMonthYear();


	const url = `https://wikimedia.org/api/rest_v1/metrics/pageviews/top/hr.wikipedia/all-access/${year}/${month}/all-days`
	var response;
	try {
		response = await client.rawRequest({
			method: 'get',
			url: url,
			responseType: 'json',
		})
	} catch (error) {
		console.log(url)
		console.log(error.message)
		res.write(init.colorText('red', 'Podaci za prošli mjesec još nisu dostupni. Molimo pokušajte sutra.'))
		return;

	}

	if (response) {
		var output = OutputData(response.data);
		output += ' ([https://hrwp.toolforge.org/najcitaniji osvježi])';

		const he = require('he');

		// Decode any encoded entities before saving
		output = he.decode(output);

		var success = await client.save('Predložak:Najčitaniji', output, 'Bot: ažuriranje');
		if (success.nochange) {
			res.write(init.colorText('red', 'Popis najčitanijih članaka već je ažuran.'))
		} else {
			if (success.result == "Success") {
				res.write(init.colorText('green', 'Popis najčitanijih članaka je uspješno ažuriran.'))
			}
		}


		if (!success.nochange) {
			let purgeStatus = await client.purge(["Predložak:Najčitaniji"], { forcerecursivelinkupdate: true })

			purgeStatus.forEach((item) => {
				if (item.purged) {
					res.write(`<br />Stranica <a href="https://hr.wikipedia.org/wiki/${item.title.replace(' ', '_')}">${item.title}</a> ` + init.colorText('green', 'uspješno je osvježena', 0) + '.');
				} else {
					res.write(`<br />Stranica <a href="https://hr.wikipedia.org/wiki/${item.title.replace(' ', '_')}">${item.title}</a> ` + init.colorText('red', 'nije uspješno osvježena', 0) + '.');
				}
			});
		}

		res.write('</div>');
	} else {
		init.generateErrorPage("Najčitaniji", "Podaci za prošli mjesec još nisu dostupni.");
		return;
	}
}

function OutputData(data) {
	//console.log(data.items[0].articles)
	excludeList = [
		'Glavna_stranica',
		'Predložak:',
		'Kategorija:',
		'Wikipedija:',
		'Posebno:',
		'wiki.phtml'
	]

	data.items[0].articles = data.items[0].articles.filter(article =>
		!excludeList.some(exclusion => article.article.includes(exclusion))
	);

	let months = ["0", "Siječanj", "Veljača", "Ožujak", "Travanj", "Svibanj", "Lipanj", "Srpanj", "Kolovoz", "Rujan", "Listopad", "Studeni", "Prosinac"];

	var out = `${months[parseInt(data.items[0].month)]} ${data.items[0].year}.: `
	for (i = 0; i < 10; i++) {
		out += `${i + 1}. [[${data.items[0].articles[i].article.replaceAll('_', ' ')}]] (${addThousandsSeparator(data.items[0].articles[i].views).replaceAll('~', '&thinsp;')})`
		if (i < 9) out += ' &bull; '
	}
	return out
}



function getPreviousMonthYear() {
	const date = new Date();
	date.setMonth(date.getMonth() - 1);

	const year = date.getFullYear();
	const month = String(date.getMonth() + 1).padStart(2, '0');

	return { year, month };
}

function addThousandsSeparator(s, separator = '~') {
	// Reverse the string to make it easier to add separators from the end
	let reversedStr = s.toString().split('').reverse().join('');

	// Add separator every 3 characters
	let withSeparators = reversedStr.replace(/(\d{3})(?=\d)/g, `$1${separator}`);

	// Reverse the string back to its original order
	return withSeparators.split('').reverse().join('');
}

module.exports = router;
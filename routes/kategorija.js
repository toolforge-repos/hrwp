const express = require('express')

const { mwn } = require('mwn');
const { default: title } = require('mwn/build/title');

var init = require('../static/init.js');

const router = express.Router()

const naslov = "Provjera kategorija";

var apiCounter = 0;

router.get("/", async (req, res) => {

	if (req.query.cat) {

		var kategorija = req.query.cat;
		var depthParam = req.query.d || "2";
		var limit = req.query.ogr || "500";

		// ako nijedno nije null
		if (!kategorija.match(regexp = "^[A-Za-z0-9ČčĆćĐđŠšŽž.,:_ -]+$")) {
			console.log('/kategorija - cat fail: ' + kategorija);
			// krivo ime
			res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
			res.write(init.generateErrorPage(naslov, 'Ime kategorije nedopuštene znakove.'));
			res.end();
			return;
		}

		if (!depthParam.match(regexp = "^[0-3]$")){
			console.log('/kategorija - depth fail: ' + depthParam);
			// krivo ime
			res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
			res.write(init.generateErrorPage(naslov, 'Nedozvoljena dubina pretrage (0-3).'));
			res.end();
			return;
		}

		if (limit.match(regexp = "^[1-9][0-9]*$") && !isNaN(limit) && limit >= 1 && limit <= 5000) {
			await generateResultPage(req, res, kategorija, limit, depthParam);
		} else {
			console.log('/kategorija - limit fail: ' + limit);
			res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
			res.write(init.generateErrorPage(naslov, 'Postavljen je neispravan limit.'));
			res.end();
			return;
		}

	} else {
		// forma
		res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
		res.write(init.generateHead(naslov));

		res.write(init.nav);

		res.write('<div class="container" style="padding-top: 10px;">')
		res.write('Unesite ime kategorije. Alat će proći po člancima, i istaknuti sve članke koji su istovremeno prisutni glavnoj kategoriji i podkategorijama:<br />');

		var form = `<form action="/kategorija" method="get">

        <label for="cat">Kategorija:</label>
        <input type="text" id="cat" name="cat" placeholder="npr. Termodinamika"/> <span> (bez prefiksa "<i>Kategorija:</i>")</span>
        <br />
		<label for="cat">Dubina pretrage (0-3):</label>
        <input type="number" id="d" name="d" value="2" min="0" max="3"/> <span> (koliko se puta rekurzivno pretražuju dohvaćene potkategorije)</span>
		<br />
        <input type="submit" value="Pretraga"></input>
        </form>`;
		res.write(form);
		res.write('<br /><b>Napomena</b>: Ovaj je postupak ' + init.colorText('red', 'vrlo intenzivan') + ', pa stranica može djelovati smrznuto dok se podaci obrađuju.')
	}
	res.end();
});


async function generateResultPage(req, res, kategorija, limit, depthParam) {
	apiUrl = 'https://hr.wikipedia.org';

	// bot account and database access credentials, if needed
	const crendentials = require('../../credentials.json');

	const strings = require("../static/strings.json");

	const client = await mwn.init({
		apiUrl: apiUrl + '/w/api.php',
		// Can be skipped if the bot doesn't need to sign in    
		username: crendentials.bot_username,
		password: crendentials.bot_password,

		// Set your user agent (required for WMF wikis, see https://meta.wikimedia.org/wiki/User-Agent_policy):    
		userAgent: init.userAgent,
		// Set default parameters to be sent to be included in every API request    
		defaultParams: {
			assert: 'user' // ensure we're logged in    
		}
	});

	// need to do either a .getSiteInfo() or .login() before we can use the client object  
	await client.getSiteInfo();
	//await client.login();

	console.log('/kategorija: ' + kategorija + '; depth: ' + depthParam + '; limit: ' + limit)

	res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
	res.write(init.generateHead(naslov));

	res.write(init.nav);

	res.write('<div class="container" style="padding-top: 10px;">')
	res.write('Alat popisuje članke koji se nalaze u nadkategoriji i nekoj od njenih podkategorija. Razmislite o uklanjanju članka iz nadkategorija.')
	apiCounter = 0;

	const duplicateArticles = await findDuplicateArticles(client, kategorija, depthParam);
	res.write('<ol>')
	var wikilink = 'https://hr.wikipedia.org/wiki';
	duplicateArticles.forEach(({ article, categories }) => {
		res.write(`<li>Članak <a href="${wikilink}/${encodeURIComponent(article)}">${article}</a>: kategorije ${categories.map(category => `<a href="${wikilink}/KT:${encodeURIComponent(category)}">${category}</a>`).join(', ')}</li>`);
	});

	res.write('</ol>')
	res.write(`Provjereno ${apiCounter} kategorija.`)

	res.write('</div>')
}

// Function to fetch category members from Wikipedia API using mwn
async function getCategoryMembers(client, category, depth, parentCategory) {
	//console.log(`Requesting ${category} at d-${depth} `)
	apiCounter++;
	const response = await client.request({
		action: 'query',
		list: 'categorymembers',
		cmtitle: `Kategorija:${category}`,
		cmlimit: 'max'
	});

	const pages = response.query.categorymembers;
	let articles = pages.filter(page => page.ns === 0).map(page => ({ title: page.title, category: parentCategory }));

	if (depth > 0) {
		for (const page of pages.filter(page => page.ns === 14)) {
			if (page.title.includes('U izradi,')) continue;
			const subcategory = page.title.substring('Kategorija:'.length);
			const subArticles = await getCategoryMembers(client, subcategory, depth - 1, subcategory);
			articles = [...articles, ...subArticles];
		}
	}

	return articles;
}


// Function to find duplicate articles
async function findDuplicateArticles(client, category, depth) {
	const articlesMap = new Map();
	const articles = await getCategoryMembers(client, category, depth, category);

	articles.forEach(({ title, category }) => {
		if (!articlesMap.has(title)) {
			articlesMap.set(title, [category]);
		} else {
			articlesMap.get(title).push(category);
		}
	});

	const duplicateArticles = Array.from(articlesMap.entries())
		.map(([article, categories]) => {
			// Remove duplicates from categories array
			const uniqueCategories = Array.from(new Set(categories));
			return { article, categories: uniqueCategories };
		})
		.filter(({ categories }) => categories.length > 1);

	return duplicateArticles;
}




module.exports = router;
const express = require('express')

const { mwn } = require('mwn');
const { default: title } = require('mwn/build/title');

var init = require('../static/init.js');

const router = express.Router()

router.get("/", (req, res) => {

	if (req.query.date) {

		var date = req.query.date;

		// ako datum nije null

		var dateParse = new Date(date);
		if (date.match(regexp = "^20(0[5-9]|1[0-9]|2[0-4])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$") && dateParse instanceof Date && !isNaN(dateParse)) {

			var dateLimit = new Date().getTime() - (31 * 24 * 60 * 60 * 1000)
			//                                      dan sat   min  sec  msec

			if (dateLimit < dateParse) {
				generateCheckPage(req, res, date);
			} else {
				console.log('/jubilarac - date over 30: ' + date);
				res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
				res.write(init.generateErrorPage('Popis jubilarnih članaka', 'Od odabranog datuma prošlo je više od 30 dana. Molimo odaberite valjani datum.'));
				res.end();
				return;
			}

		} else {
			console.log('/jubilarac - date fail: ' + date);
			res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
			res.write(init.generateErrorPage('Popis jubilarnih članaka', 'Datum nije ispravnog oblika.'));
			res.end();
			return;
		}

	} else {
		// date je null
		// forma
		res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
		res.write(init.generateHead('Popis jubilarnih članaka'));

		res.write(init.nav);

		res.write('<div class="container" style="padding-top: 10px;">')
		res.write('Popis jubilarnih članaka na Wikipediji na hrvatskome jeziku:<br />');

		var form = `<form action="/jubilarac" method="get">

        <label for="date">Datum izbora jubilarnog članka:</label>
        <select id="date" name="date">`;
		res.write(form);

		form = '';

		for (var i = 0; i < 10; i++) {
			var date = new Date();
			date.setDate(date.getDate() - i);
			form = form + '<option value="' + date.toISOString().split('T')[0] + '">' + date.toLocaleDateString('hr-HR') + '</option>';
		}

		form = form + `</select><br>
        <input type="submit" value="Izradi popis jubilarnih članaka"></input>
        </form>`;
		res.write(form);

		res.end();
	}
});


async function generateCheckPage(req, res, dateParse) {
	apiUrl = 'https://hr.wikipedia.org';

	// bot account and database access credentials, if needed
	const crendentials = require('../../credentials.json');

	const strings = require("../static/strings.json");

	const client = await mwn.init({
		apiUrl: apiUrl + '/w/api.php',
		// Can be skipped if the bot doesn't need to sign in    
		username: crendentials.bot_username,
		password: crendentials.bot_password,

		// Set your user agent (required for WMF wikis, see https://meta.wikimedia.org/wiki/User-Agent_policy):    
		userAgent: init.userAgent,
		// Set default parameters to be sent to be included in every API request    
		defaultParams: {
			assert: 'user' // ensure we're logged in    
		}
	});

	// need to do either a .getSiteInfo() or .login() before we can use the client object  
	await client.getSiteInfo();
	//await client.login();

	console.log('/jubilarac: ' + dateParse);

	res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
	res.write(init.generateHead('Popis jubilarnih članaka', 0, 1));

	res.write(init.nav);

	res.write('<div class="container" style="padding-top: 10px;">');



	var dateInput = dateParse;

	const { DateTime } = require('luxon');


	// Create a DateTime object in the 'Europe/Zagreb' timezone
	var zagrebTimezone = 'Europe/Zagreb';
	var dateInZagreb = DateTime.fromFormat(dateInput, 'yyyy-MM-dd', { zone: zagrebTimezone }).startOf('day');

	// Create dateStart and dateEnd relative to 'Europe/Zagreb' timezone
	var dateStart = dateInZagreb;
	var dateEnd = dateInZagreb.endOf('day');

	// TODO Nastavak


	res.write('Jubilarac za raspon ' + dateStart.toISO() + ' do ' + dateEnd.toISO())

	var jubilaracVelikiArray = [];
	var jubilaracMaliArray = [];
	var jubilaracSumnjiviArrayPrvi = [];
	var jubilaracSumnjiviArrayDrugi = [];

	var problemi = [
		"Kategorija:Razdvojba",
		"Kategorija:Članci koje treba dodatno razjasniti",
		"Kategorija:Članci koje treba provjeriti",
		"Kategorija:Članci koji trebaju izvor",
		"Kategorija:Članci kojima nedostaje izvor",
		"Kategorija:Članci kojima nedostaju slike",
		"Kategorija:Članci predloženi za brisanje",
		"Kategorija:Članci predloženi za brzo brisanje",
		"Kategorija:Nacrti članaka automatski predloženi za brisanje",
		"Kategorija:Nacrti članaka",
		"Kategorija:Članci predloženi za podjelu",
		"Kategorija:Članci predloženi za spajanje",
		"Kategorija:Članci prevedeni strojnim prijevodom",
		"Kategorija:Članci s neaktivnim poveznicama",
		"Kategorija:Članci s odjeljcima koji su van teme",
		"Kategorija:Članci u koje treba uklopiti drugi tekst",
		"Kategorija:Članci u kojima je potrebno provjeriti točnost",
		"Kategorija:Članci u kojima treba nešto dodatno pojasniti",
		"Kategorija:Izdvojiti u novi članak",
		"Kategorija:Izdvoji članke iz razdvojbe",
		"Kategorija:Kriva transliteracija",
		"Kategorija:Nedostaje infookvir",
		"Kategorija:Nedostaju dijakritici",
		"Kategorija:Nedostaju wikipoveznice",
		"Kategorija:Potreban bolji naslov",
		"Kategorija:Potreban prijevod",
		"Kategorija:Potreban prijevod s mađarskog jezika",
		"Kategorija:Potreban prijevod s albanskog jezika",
		"Kategorija:Potreban prijevod s bošnjačkog jezika",
		"Kategorija:Potreban prijevod s crnogorskog jezika",
		"Kategorija:Potreban prijevod s češkog jezika",
		"Kategorija:Potreban prijevod s engleskog jezika",
		"Kategorija:Potreban prijevod s francuskog jezika",
		"Kategorija:Potreban prijevod s nizozemskog jezika",
		"Kategorija:Potreban prijevod s njemačkog jezika",
		"Kategorija:Potreban prijevod s portugalskog jezika",
		"Kategorija:Potreban prijevod s ruskog jezika",
		"Kategorija:Potreban prijevod s talijanskog jezika",
		"Kategorija:Potreban prijevod sa slovenskog jezika",
		"Kategorija:Potreban prijevod sa srpskog jezika",
		"Kategorija:Potreban prijevod sa španjolskog jezika",
		"Kategorija:Potreban prijevod sa švedskog jezika",
		"Kategorija:Potreban prijevod sa slovačkog jezika",
		"Kategorija:Potrebna kategorizacija",
		"Kategorija:Potrebna provjera kriterija",
		"Kategorija:Potrebna razdvojba",
		"Kategorija:Potrebna stilska dorada",
		"Kategorija:Potrebno dopuniti botom postavljeni predložak",
		"Kategorija:Potrebno ispraviti poredak riječi u rečenici",
		"Kategorija:Potrebno ispraviti stil izvora",
		"Kategorija:Potrebno napraviti predložak",
		"Kategorija:Potrebno pohrvatiti imena",
		"Kategorija:Potrebno popraviti stil pisanja",
		"Kategorija:Potrebno popraviti wikipoveznice",
		"Kategorija:Potrebno provjeriti vanjske poveznice",
		"Kategorija:Potrebno wikipedizirati",
		"Kategorija:Potrebno wikipedizirati izvore",
		"Kategorija:Premjestiti na Zajednički poslužitelj",
		"Kategorija:Premjestiti u Wikicitat",
		"Kategorija:Premjestiti u Wikiknjige",
		"Kategorija:Premjestiti u Wikizvor",
		"Kategorija:Premjestiti u Wječnik",
		"Kategorija:Stranice s nepregledanim prijevodima",
		"Kategorija:Članci s konfliktom interesa",
		"Kategorija:Jednostrani prikaz teme",
		"Kategorija:Nepristranost osporena",
		"Kategorija:Originalne ideje",
		"Kategorija:Potrebno provjeriti izvor",
		"Kategorija:Prepisano bez dozvole",
		"Kategorija:Točnost osporena",
		"Kategorija:Tekstovi koje treba uklopiti u druge članke",
		"Kategorija:Upitna značajnost",
		"Kategorija:Wikiprojekt neutralnost, srediti do zadanog roka",
		"Kategorija:Zastarjeli podaci",
	]
	// Auto-popisivanje kategorija za neodgovarajući sadržaj po datumima

	await client.request(
		{
			"action": "query",
			"format": "json",
			"list": "categorymembers",
			"formatversion": "2",
			"cmtitle": "Kategorija:Neodgovarajući sadržaj",
			"cmprop": "title",
			"cmtype": "subcat",
			"cmlimit": "max"
		}).then(async function (data) {
			data.query.categorymembers.forEach(function (e) {
				problemi.push(e.title);
			})
		});

	await client.request({
		"action": "query",
		"format": "json",
		"prop": "info|categories",
		"inprop": "talkid",
		"generator": "recentchanges",
		"utf8": 1,
		"grcstart": dateStart.toISO(),
		"grcend": dateEnd.toISO(),
		"grcdir": "newer",
		"grcnamespace": "0|102",
		"grcprop": "title|timestamp|ids|sizes",
		"grclimit": "max",
		"grctype": "new",
		"clcategories": problemi.join('|')
	}).then(async function (data) {
		var jubID = Object.keys(data.query.pages);
		for (var i = 0; i < jubID.length; i++) {

			if (parseInt(data.query.pages[jubID[i]].length) >= 3000) {
				jubilaracVelikiArray.push(data.query.pages[jubID[i]]);
			} else if (parseInt(data.query.pages[jubID[i]].length) < 3000) {
				jubilaracMaliArray.push(data.query.pages[jubID[i]]);
			} else {
				jubilaracSumnjiviArrayPrvi.push(data.query.pages[jubID[i]]);
			}
		}

		jubilaracVelikiArray = jubilaracVelikiArray.filter(e => !(e.hasOwnProperty('redirect')));
		jubilaracMaliArray = jubilaracMaliArray.filter(e => !(e.hasOwnProperty('redirect')));

		// ukloni razdvojbe
		jubilaracVelikiArray = jubilaracVelikiArray.filter(obj => {
			if (obj.categories) {
				return !obj.categories.some(category => category.title === "Kategorija:Razdvojba");
			}
			// If "categories" doesn't exist, keep the object
			return true;
		});


		jubilaracMaliArray = jubilaracMaliArray.filter(obj => {
			if (obj.categories) {
				return !obj.categories.some(category => category.title === "Kategorija:Razdvojba");
			}
			// If "categories" doesn't exist, keep the object
			return true;
		});
		
		jubilaracVelikiArray.sort(((a, b) => (a.title > b.title) ? 1 : -1));

		jubilaracMaliArray.sort(((a, b) => (a.title > b.title) ? 1 : -1));
		jubilaracSumnjiviArrayPrvi.sort(((a, b) => (a.title > b.title) ? 1 : -1));



		// ispis

		a = '</div><hr><div class="container-fluid">'
		a = a + '<div class="row row-one">'
		a = a + '<div class="col-left col-lg-6 col-md-12">' + printJub(jubilaracVelikiArray, 'Članci sa VIŠE od 3000 bajtova') + '</div>'
		a = a + '<div class="col-right col-lg-6 col-md-12">' + printJub(jubilaracMaliArray, "Članci sa MANJE od 3000 bajtova") + '</div></div>'

		res.write(a);

		res.write("Izmjene označene <span style='background-color: pink;'>ovako</span> prisutne su u barem jednoj kategoriji za održavanje (");
		res.write(init.mnozina(problemi.length, 'prati se @ kategorija', 'prate se @ kategorije', 'prati se @ kategorija') + ')');

		res.write('</div><hr><div class="container"><h2 style="text-align: center;">Wikitekst:</h2>')
		res.write(await generateWikitext(jubilaracVelikiArray, jubilaracMaliArray, dateParse));
		res.write('</div>');

	})
		.finally(() => {
			res.write('<div style="padding-top: 20px;">Provjera završena!</div></body>');
		}).catch(err => { init.failureCallback(err, req, res); });

	res.end();
}

function printJub(array, naslov) {
	var lang = 'hr';
	var a = '<div class="tablediv">'
	if (array.length > 0) {
		a = a + `<table class="patrol table table-hover table-sm table-striped sortable">
            <thead>
            <div colspan="5" style="text-align: center;" class="sticky">` + naslov + ` (` + array.length + `)</div>
            <tr class="header-sortable">
            <th scope="col"># članka</th>
            <th scope="col">` + init.getTranslation(lang, "tabTitle") + `</th>
            <th scope="col">` + init.getTranslation(lang, "tabSize") + `</th>
            <th scope="col">Zadnja izmjena</th>`
		if (array[0].ores && !array[0].ores.reverted.error) {
			a = a + '<th scope="col">' + "ORES" + '</th>'
		}
		a = a + `</tr>
            </thead>
            <tbody>`;


		array.forEach(function (e) {
			a = a + '<tr';
			if (e.hasOwnProperty('categories')) a = a + ' style="background-color: pink;"';
			a = a + '>'

			a = a + '<th scope="row"><a target="_blank" href="' + apiUrl + '/w/index.php?curid=' + e.pageid + '">' + e.pageid + '</a></th>'
				+ '<td style ="word-break:break-word;">' + e.title

				+ ' (';
			(e.hasOwnProperty('talkid') ? a = a + '<a target="_blank" href="' + apiUrl + '/w/index.php?curid=' + e.talkid + '">razg</a> &middot; ' : '');

			a = a + '<a target="_blank" href="' + apiUrl + '/w/index.php?title=' + e.title + '&action=history">'
				+ init.getTranslation(lang, 'historyShort') + '</a>)</td>'

			a = a + printDiffLength(e.length);
			a = a + '<td sorttable_customkey="' + e.touched + '">' +

				'<a target="_blank" href="' + apiUrl + '/wiki/Special:Diff/' + e.lastrevid + '">' + shortDate(init.getTranslation(lang, "monthNamesShort"), e.touched) + '</a></td>'


			a
				+ '</tr>'
		});
		a = a + '</tbody></table>'
	}
	else {
		a = a + `<div colspan="5" style="text-align: center;" class="sticky">` + naslov + `</div>`;
		a = a + `<div colspan="5" style="text-align: center;" class="nochanges">` + init.getTranslation(lang, "noEdits") + `</div>`;
	}
	a = a + '</div>'
	return a;
}

async function generateWikitext(veliki, mali, datum) {

	var mj = init.getTranslation('hr', "monthNames")

	var date = new Date(new Date(datum).setHours(15));
	var a = '<textarea readonly="" style="width:100%; max-width:800px; height:100%; margin: 0 auto;display: block;">';
	a = a + '== Novi članci započeti ' + date.getDate() + '. ' + mj[date.getMonth()] + ' (abecednim redom) ==\n';

	veliki.forEach(function (e) {
		a = a + '=== [[' + e.title + ']] ===\n\n'
	})

	a = a + '== Novi članci započeti ' + date.getDate() + '.  ' + mj[date.getMonth()] + ' manji od 3000 bajtova (abecednim redom) ==\n';

	mali.forEach(function (e) {
		a = a + '=== [[' + e.title + ']] ===\n\n'
	})



	a = a + '</textarea>';



	return a;
}


function shortDate(monthNames, datumRaw) {
	var datum = new Date(datumRaw);
	return datum.getDate() + '. ' + monthNames[datum.getMonth()] + '. u ' + datum.getHours() + ':' + datum.getMinutes();
}


function formatNum(num) {
	return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function printDiffLength(num) {
	var a = '<td style="';
	// color
	num == 0 ? '' :
		num < 0 ? a = a + 'color: red;' : a = a + 'color:green;';

	//bold
	(num > 499 || num < -499) ? a = a + 'font-weight:bold;' : '';

	a = a + '" sorttable_customkey="' + num + '">(';

	//sign
	num > 0 ? a = a + '+' : '';

	a = a + formatNum(num) + ')</td>';
	return a;
}


module.exports = router;
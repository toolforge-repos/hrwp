const express = require('express')

const { mwn } = require('mwn');
const { default: title } = require('mwn/build/title');

var init = require('../static/init.js');

const router = express.Router()

router.get("/", (req, res) => {
    generateDraftPage(req, res);
});

async function generateDraftPage(req, res) {
    apiUrl = 'https://hr.wikipedia.org';

    // bot account and database access credentials, if needed
    const crendentials = require('../../credentials.json');

    const strings = require("../static/strings.json");

    const client = await mwn.init({
        apiUrl: apiUrl + '/w/api.php',
        // Can be skipped if the bot doesn't need to sign in    
        username: crendentials.bot_username,
        password: crendentials.bot_password,

        // Set your user agent (required for WMF wikis, see https://meta.wikimedia.org/wiki/User-Agent_policy):    
        userAgent: init.userAgent,
        // Set default parameters to be sent to be included in every API request    
        defaultParams: {
            assert: 'user' // ensure we're logged in    
        }
    });

    // need to do either a .getSiteInfo() or .login() before we can use the client object  
    await client.getSiteInfo();
    //await client.login();

	console.log('/nacrt');

    res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
    res.write(init.generateHead('Provjera nacrta'));

    res.write(init.nav);

    res.write('<div class="container" style="padding-top: 10px;">')

    //dohvati sve stranice u nacrtu

    var imaNacrt = [];
    var nemaNacrt = [];

    // dohvati kategorije stranica u nacrtu koje imaju kategoriju

    await client.request({
        "action": "query",
        "format": "json",
        "prop": "categories",
        "generator": "allpages",
        "formatversion": "2",
        "cllimit": "max",
        "clcategories": "Kategorija:Nacrti članaka|Kategorija:Nacrti članaka u krivom imenskom prostoru",
        "gapnamespace": "118",
        "gaplimit": "max",
        "gapfilterredir": "nonredirects"
    }).then(async function (data) {
        data.query.pages.forEach(function (e) {
            if ('categories' in e) {
                imaNacrt.push(e);
            } else {
                nemaNacrt.push(e);
            }
        });
    });

    if (nemaNacrt.length == 0){
        res.write('Sve stranice imaju nacrt (');
		res.write(init.mnozina(imaNacrt.length, 'provjerena @ stranica', 'provjerene @ stranice', 'provjereno @ stranica') + ')');
    } else {
        res.write('<p>Stranice bez predloška Nacrt:</p><ol>');
        nemaNacrt.forEach(function(e){
            res.write('<li><a href="https://hr.wikipedia.org/wiki/' + e.title + '">' + e.title + '</a></li>');
        })
    }
    res.end();
};



module.exports = router;
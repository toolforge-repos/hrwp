const express = require('express')

const { mwn } = require('mwn');
const { default: title } = require('mwn/build/title');

var init = require('../static/init.js');

const router = express.Router()

router.get("/", (req, res) => {
    generatePopisForm(req, res);
});

router.post("/", (req, res) => {
    provjeriPodatke(req, res);

});


async function generatePopisForm(req, res) {
    res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
    res.write(init.generateHead('Popis stanovništva'));

    res.write(init.nav);

    res.write('<div class="container" style="padding-top: 10px;">');
    var forma = `<form action="/popis" method="post">
    
    <label for="zupanija">Country:</label>
    <select id="zupanija" name="zupanija">
        <option value="BBŽ">Bjelovarsko-bilogorska (BBŽ)</option>
        <option value="BPŽ">Brodsko-posavska (BPŽ)</option>
        <option value="DNŽ">Dubrovačko-neretvanska (DNŽ)</option>
        <option value="GZG">Grad Zagreb (GZG)</option>
        <option value="ISŽ">Istarska (ISŽ)</option>
        <option value="KAŽ">Karlovačka (KAŽ)</option>
        <option value="KKŽ">Koprivničko-križevačka (KKŽ)</option>
        <option value="KZŽ">Krapinsko-zagorska (KZŽ)</option>
        <option value="LSŽ">Ličko-senjska (LSŽ)</option>
        <option value="MEŽ">Međimurska (MEŽ)</option>
        <option value="OBŽ">Osječko-baranjska (OBŽ)</option>
        <option value="PGŽ">Primorsko-goranska (PGŽ)</option>
        <option value="PSŽ">Požeško-slavonska (PSŽ)</option>
        <option value="SDŽ">Splitsko-dalmatinska (SDŽ)</option>
        <option value="SMŽ">Sisačko-moslavačka (SMŽ)</option>
        <option value="VPŽ">Virovitičko-podravska (VPŽ)</option>
        <option value="VSŽ">Vukovarsko-srijemska (VSŽ)</option>
        <option value="VŽŽ">Varaždinska (VŽŽ)</option>
        <option value="ZDŽ">Zadarska (ZDŽ)</option>
        <option value="ZGŽ">Zagrebačka (ZGŽ)</option>
        <option value="ŠKŽ">Šibensko-kninska (ŠKŽ)</option>
    </select>
    <br>
    
    <label for="name">Naselje:</label>
    <input type="text" id="naselje" name="naselje" required>
    <br>

    <label for="tekst">Tekst:</label>
    <br>
    <textarea id="tekst" name="tekst" rows="10" cols="70" required></textarea>
    <br>

    <input type="submit" value="Submit">
</form>`
    res.write(forma + '</div>');
    res.end();

}


async function provjeriPodatke(req, res) {
    let zupanija = req.body.zupanija;
    let naselje = req.body.naselje;
    let tekst = req.body.tekst;

    var failFlag = 0;
    var bugText = '';

    const dozvoljeneZupanije = ["BBŽ", "BPŽ", "DNŽ", "GZG", "ISŽ",
        "KAŽ", "KKŽ", "KZŽ", "LSŽ", "MEŽ",
        "OBŽ", "PGŽ", "PSŽ", "SDŽ", "SMŽ",
        "VPŽ", "VSŽ", "VŽŽ", "ZDŽ", "ZGŽ", "ŠKŽ"];

    const zupanijaMatch = dozvoljeneZupanije.find(code => code === zupanija);
    if (!zupanijaMatch) {
        bugText += "<p>Pogrešna županija... Pa kak?!</p>";
        failFlag = 1;
    }

    if (!/^[A-ž]{1,10}(\s[A-ž]{1,10})?$/.test(naselje)) {
        bugText += "<p>Pogreška u imenu naselja.</p>";
        failFlag = 1;
    }

    if (tekst !== null && tekst !== undefined && typeof tekst === "string" && tekst.length > 16) {
        const tabCount = (tekst.split('\t').length - 1);

        if (!tabCount || tabCount % 15 !== 0 || tabCount < 15) {
            bugText += "<p>Pogreška u oblikovanju teksta. Tekst treba imati 15 tabulatora po retku.</p>";
            failFlag = 1;
        }
    } else {
        bugText += "<p>Pogreška: tekst nije dovoljno velik.</p>";
            failFlag = 1;
    }

    if (failFlag === 0) {
        ExecutePopis(req, res);
    } else {
        res.write(init.generateErrorPage("Popis stanovništva - greška", bugText));
        res.end();
    }

}



async function ExecutePopis(req, res) {
    apiUrl = 'https://hr.wikipedia.org';

    // bot account and database access credentials, if needed
    const crendentials = require('../../credentials.json');

    const strings = require("../static/strings.json");

    const client = await mwn.init({
        apiUrl: apiUrl + '/w/api.php',
        // Can be skipped if the bot doesn't need to sign in    
        username: crendentials.bot_username,
        password: crendentials.bot_password,

        // Set your user agent (required for WMF wikis, see https://meta.wikimedia.org/wiki/User-Agent_policy):    
        userAgent: init.userAgent,
        // Set default parameters to be sent to be included in every API request    
        defaultParams: {
            assert: 'user' // ensure we're logged in    
        }
    });

    // need to do either a .getSiteInfo() or .login() before we can use the client object  
    await client.getSiteInfo();
    //await client.login();

	console.log('/popis')

    res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
    res.write(init.generateHead('Popis stanovništva'));

    res.write(init.nav);

    res.write('<div class="container" style="padding-top: 10px;">')

    res.write('test pass');

    let zupanija = req.body.zupanija;
    let naselje = req.body.naselje;
    let tekst = req.body.tekst;




    var linije = tekst.split('\n');
    for (i=0; i<linije.length; i++) {
        console.log(linije[i].split('\t')[0]);
    }


    res.write('</div>')
    res.end();
};



module.exports = router;
const express = require('express')

const { mwn } = require('mwn');
const { default: title } = require('mwn/build/title');

var init = require('../static/init.js');

var https = require('https');


const router = express.Router()

var lang = 'undefined';
var projekt = 'undefined';

router.get("/:projekt", async (req, res) => {
	var projekti = ["ar", "de", "es", "hr", "nl", "sh", "sv"]

	var specialProjects = ["wikidata"];

	projekt = req.params.projekt;
	var limit = 1000;
	var logsLimit = 1000;

	if (projekti.includes(projekt) || specialProjects.includes(projekt)) {
		await generatePatrolPage(projekt, req, res, specialProjects.includes(projekt), limit, logsLimit);
	} else {
		console.log('/patrol - projekt fail: ' + projekt);
		res.write(init.generateErrorPage("Patrol status", "Unsupported project."));
		res.end();
		return;
	}
	res.end();
});

async function generatePatrolPage(projekt, req, res, special, limit, logsLimit) {
	lang = projekt;

	apiUrl = 'https://' + projekt + '.wikipedia.org';
	if (special) {
		lang = 'en';
		switch (projekt) {
			case 'wikidata': apiUrl = 'https://www.wikidata.org'; break;
			case 'commons': apiUrl = 'https://commons.wikimedia.org'; break;
		}
	}

	// bot account and database access credentials, if needed
	const crendentials = require('../../credentials.json');

	const client = await mwn.init({
		apiUrl: apiUrl + '/w/api.php',
		// Can be skipped if the bot doesn't need to sign in    
		username: crendentials.bot_username,
		password: crendentials.bot_password,

		// Set your user agent (required for WMF wikis, see https://meta.wikimedia.org/wiki/User-Agent_policy):    
		userAgent: init.userAgent,
		// Set default parameters to be sent to be included in every API request    
		defaultParams: {
			assert: 'user' // ensure we're logged in    
		}
	});

	// need to do either a .getSiteInfo() or .login() before we can use the client object  
	await client.getSiteInfo();
	//await client.login();

	console.log(`/patrol: ${projekt} (${lang})`)

	res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
	res.write(init.generateHead(init.getTranslation(lang, "webtitle"), 0, 1));

	res.write(init.nav);

	res.write('<div class="container" style="padding-top: 10px;">')

	await generatePreface(req, res, client);

	let { izmjene_ip, izmjene, stranice_ip, stranice } = await getUnpatrolledEdits(client, limit);

	sortArray(izmjene_ip);
	sortArray(izmjene);
	sortArray(stranice_ip);
	sortArray(stranice);

	let sum = izmjene_ip.length + izmjene.length + stranice_ip.length + stranice.length;

	// lista ukupnih izmjena

	var a = init.getTranslation(lang, "unpatrolled-top", '<b>' + sum + '</b>');

	a = a + '<br><br>'

	a = a + init.getTranslation(lang, "loggedIn") + ' (<a href="' + apiUrl + '/w/index.php?title=Special:RecentChanges&days=30&from=&limit=5000&hideanons=1&hidepatrolled=1">' + init.getTranslation(lang, "seeInRC") + '</a>): <ul>'
	a = a + neophodjenoTekst(izmjene, false);
	a = a + neophodjenoTekst(stranice, true);
	a = a + '</ul>'
	a = a + init.getTranslation(lang, "loggedOut") + ' (<a href="' + apiUrl + '/w/index.php?title=Special:RecentChanges&days=30&from=&limit=5000&hideliu=1&hidepatrolled=1">'
	a = a + init.getTranslation(lang, "seeInRC") + '</a>): <ul>'
	a = a + neophodjenoTekst(izmjene_ip, false);
	a = a + neophodjenoTekst(stranice_ip, true);
	a = a + '</ul>'

	res.write(a);

	//tablice

	res.write(init.getTranslation(lang, "markedEntries", "<span style='background-color: pink;'>", "</span>"));
	a = '</div><hr><div class="container-fluid">'
	a = a + '<div class="row row-one">'
	a = a + '<div class="col-left col-lg-6 col-md-12">' + prikaziPromjene(izmjene, init.getTranslation(lang, "loggedIn") + " - " + init.getTranslation(lang, "edits")) + '</div>';
	a = a + '<div class="col-right col-lg-6 col-md-12">' + prikaziPromjene(stranice, init.getTranslation(lang, "loggedIn") + " - " + init.getTranslation(lang, "newPages")) + '</div></div>';
	a = a + '<div class="row row-two">'
	a = a + '<div class="col-left col-lg-6 col-md-12">' + prikaziPromjene(izmjene_ip, init.getTranslation(lang, "loggedOut") + " - " + init.getTranslation(lang, "edits")) + '</div>';
	a = a + '<div class="col-right col-lg-6 col-md-12">' + prikaziPromjene(stranice_ip, init.getTranslation(lang, "loggedOut") + " - " + init.getTranslation(lang, "newPages")) + '</div></div>';


	res.write(a)
	res.write('</div>')

	var d = new Date();
	d.setDate(d.getDate() - 3);

	let bestPatrollers = await getPatrolLogs(client, logsLimit, d);

	res.write('</div><hr><div class="container">');

	res.write('<b>' + init.getTranslation(lang, "mostPatrolsText") + ':</b> (' + init.getTranslation(lang, "sinceDate") + " " + konverzijaDatuma(init.getTranslation(lang, "jsLocaleLang"), init.getTranslation(lang, "jsLocaleTimeZone"), init.getTranslation(lang, "monthNames"), d.toISOString()) + ')<ol>');


	const zbroj_ophodnji = Object.values(bestPatrollers).reduce((total, amount) => total + amount, 0);

	const sortedSummary = Object.entries(bestPatrollers)
		.sort((a, b) => b[1] - a[1])
		.reduce((obj, [key, value]) => ({ ...obj, [key]: value }), {});


	Object.entries(sortedSummary).forEach(([user, amount]) => {
		res.write('<li>' + user + ' - ' + amount + ' ' + init.getTranslation(lang, "patrols") + '</li>')
	});

	res.write('<span class="total">' + init.getTranslation(lang, "totalPatrols") + ' - ' + zbroj_ophodnji + ' ' + init.getTranslation(lang, "patrols") + '</span>');
	res.write('</ol></div></html>');
}


function generatePreface(req, res, client) {
	res.write(init.getTranslation(lang, "todayIs") + " " + konverzijaDatuma(init.getTranslation(lang, "jsLocaleLang"), init.getTranslation(lang, "jsLocaleTimeZone"), init.getTranslation(lang, "monthNames"), new Date()) + '<br><br>')

	client.request({
		"action": "query",
		"format": "json",
		"meta": "siteinfo",
		"siprop": "dbrepllag|statistics"
	}).then((data) => {


		res.write(init.getTranslation(lang, "projectStats", '<b>' + projekt + 'wiki</b>', formatNum(data.query.statistics.pages), formatNum(data.query.statistics.articles)) + '<br><br>');

		res.write(init.getTranslation(lang, "replag", data.query.dbrepllag[0].lag, data.query.dbrepllag[0].host) + '<br><br>')
	})
		.catch(err => { init.failureCallback(err, req, res); });
}

function konverzijaDatuma(localeLang, jsLocaleTimeZone, monthNames, datumRaw) {
	var datum = new Date(datumRaw);
	var weekday = datum.toLocaleString(
		localeLang, {
		weekday: 'long',
		timeZone: jsLocaleTimeZone
	});
	return weekday + ', ' + datum.getDate() + '. ' + monthNames[datum.getMonth()] + ' ' + datum.getFullYear()
		+ '. ' + datum.toLocaleTimeString(
			localeLang, {
			timeZone: jsLocaleTimeZone
		});
}

function formatNum(num) {
	return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

async function getUnpatrolledEdits(client, limit) {
	var izmjene_ip = [];
	var izmjene = [];
	var stranice_ip = [];
	var stranice = [];

	let remaining = limit;
	let params = {
		"action": "query",
		format: "json",
		list: "recentchanges",
		utf8: 1,
		rcdir: "newer",
		rcprop: "patrolled|user|timestamp|title|ids|sizes|comment|parsedcomment",
		rcshow: "unpatrolled",
	}

	while (remaining > 0) {
		params.rclimit = Math.min(500, remaining);

		let data = await client.request(params);

		let pagesRetrieved = 0;
		data.query.recentchanges.forEach((e) => {
			if (e.hasOwnProperty('title')) {

				if ("anon" in e) {
					if (e.type == 'edit') {
						izmjene_ip.push(e);
					} else {
						stranice_ip.push(e);
					}
				}
				else {
					if (e.type == 'edit') {
						izmjene.push(e);
					} else {
						stranice.push(e);
					}
				}

				pagesRetrieved++;
			}
		});

		remaining -= pagesRetrieved;

		// set new params
		delete params.rccontinue;
		delete params.rvcontinue;


		if (!data.hasOwnProperty('continue')) {
			break;
		} else { // more than 12 MB
			if (data.continue.hasOwnProperty('rvcontinue')) {
				params.rvcontinue = data.continue.rvcontinue; //?
			} else if (data.continue.hasOwnProperty('rccontinue')) {
				params.rccontinue = data.continue.rccontinue;
			}
		}
	}

	return { izmjene_ip, izmjene, stranice_ip, stranice };
}

function neophodjenoTekst(array, str) {
	var retval = ''
	if (str == true) {
		retval = '<li>' + init.getTranslation(lang, "countUnpPages", '<b>' + array.length + '</b>');
		if (array.length > 0) {
			retval = retval + ' (' + init.getTranslation(lang, "oldestNom") + " " + konverzijaDatuma(init.getTranslation(lang, "jsLocaleLang"), init.getTranslation(lang, "jsLocaleTimeZone"), init.getTranslation(lang, "monthNames"), array[0].timestamp)
				+ ' -- <a target="_blank" href="' + apiUrl + '/wiki/Special:Diff/' + array[0].revid + '">'
				+ init.getTranslation(lang, "linkHere") + '</a>)</li>'
		}
	}
	else {
		retval = '<li>' + init.getTranslation(lang, "countUnpEdits", '<b>' + array.length + '</b>');
		if (array.length > 0) {
			retval = retval + ' (' + init.getTranslation(lang, "oldestGen") + " " + konverzijaDatuma(init.getTranslation(lang, "jsLocaleLang"), init.getTranslation(lang, "jsLocaleTimeZone"), init.getTranslation(lang, "monthNames"), array[0].timestamp)
				+ ' -- <a target="_blank" href="' + apiUrl + '/wiki/Special:Diff/' + array[0].revid + '">'
				+ init.getTranslation(lang, "linkHere") + '</a>)</li>'
		}
	}
	return retval;
}

function sortArray(array) {
	array.sort(function (a, b) {
		return (a.revid < b.revid) ? -1 : ((a.revid > b.revid) ? 1 : 0);
	});
}

function prikaziPromjene(array, naslov) {
	var a = '<div class="tablediv">'
	if (array.length > 0) {
		a = a + `<table class="patrol table table-hover table-sm table-striped sortable">
		<thead>
		<div colspan="5" style="text-align: center;" class="sticky">` + naslov + ` (` + array.length + `)</div>
		<tr class="header-sortable">
		<th scope="col">` + init.getTranslation(lang, "tabDiff") + `</th>
		<th scope="col">` + init.getTranslation(lang, "tabTitle") + `</th>
		<th scope="col">` + init.getTranslation(lang, "tabUser") + `</th>
		<th scope="col">` + init.getTranslation(lang, "tabSize") + `</th>
		<th scope="col">` + init.getTranslation(lang, "tabDate") + `</th>`
		if (array[0].ores && !array[0].ores.reverted.error) {
			a = a + '<th scope="col">' + "ORES" + '</th>'
		}
		a = a + `</tr>
		</thead>
		<tbody>`;

		var dateLimit = new Date();
		dateLimit.setDate(dateLimit.getDate() - 30);
		dateLimit.setHours(23, 59, 59, 59);

		array.forEach(function (e) {
			var myDate = new Date(e.timestamp);
			a = a + '<tr';
			if (myDate < dateLimit) a = a + ' class="pink"';
			a = a + '>'

			a = a + '<th scope="row"><a target="_blank" href="' + apiUrl + '/wiki/Special:Diff/' + e.revid + '">' + e.revid + '</a></th>'
				+ '<td style ="word-break:break-word;">';

			let comment = '';
			let commentLen = escapeComment(e.parsedcomment).toString().length;

			if (commentLen > 0) {
				comment = '<abbr title=" ' + escapeComment(e.parsedcomment) + '">' + e.title + '</abbr>';
			} else { comment = e.title; }

			a = a + comment + ' (<a target="_blank" href="' + apiUrl + '/w/index.php?title=' + e.title + '&action=history">'
				+ init.getTranslation(lang, 'historyShort') + '</a>)</td>'

				+ '<td style ="word-break:break-word;">' + e.user
				+ ' (<a target="_blank" href="' + apiUrl + '/wiki/User_talk:' + e.user + '">'
				+ init.getTranslation(lang, 'userTalkShort') + '</a> &middot; '

				+ '<a target="_blank" href="' + apiUrl + '/wiki/Special:Contributions/' + e.user + '">'
				+ init.getTranslation(lang, 'userContribsShort') + '</a>)</td>';

			a = a + printDiffLength((e.newlen - e.oldlen));
			a = a + '<td sorttable_customkey="' + e.timestamp + '">' + shortDate(init.getTranslation(lang, "monthNamesShort"), e.timestamp) + '</td>'
			if (array[0].ores && !array[0].ores.reverted.error) {
				a = a + printOresScores(e.ores);
			}
			+ '</tr>'
		});
		a = a + '</tbody></table>'
	}
	else {
		a = a + `<div colspan="5" style="text-align: center;" class="sticky">` + naslov + `</div>`;
		a = a + `<div colspan="5" style="text-align: center;" class="nochanges">` + init.getTranslation(lang, "noEdits") + `</div>`;
	}
	a = a + '</div>'
	return a;
}

function escapeComment(comment) {
	let str = init.escapeSpecials(comment);
	let pattern = /&lt;[\s\S]*?&gt;/g;
	let ret = str.replace(pattern, '').trim();
	return ret;
}

function printDiffLength(num) {
	var a = '<td style="';
	// color
	num == 0 ? '' :
		num < 0 ? a = a + 'color: red;' : a = a + 'color:green;';

	//bold
	(num > 499 || num < -499) ? a = a + 'font-weight:bold;' : '';

	a = a + '" sorttable_customkey="' + num + '">(';

	//sign
	num > 0 ? a = a + '+' : '';

	a = a + formatNum(num) + ')</td>';
	return a;
}

function formatNum(num) {
	return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function shortDate(monthNames, datumRaw) {
	var datum = new Date(datumRaw);
	return datum.getDate() + '. ' + monthNames[datum.getMonth()];
}

async function getPatrolLogs(client, limit, d) {
	let summary = {};
	let remaining = limit;
	let params = {
		"action": "query",
		"format": "json",
		"list": "logevents",
		"utf8": 1,
		"leprop": "user|type",
		"leaction": "patrol/patrol",
		"lestart": d.toISOString(),
		"ledir": "newer",
	}

	while (remaining > 0) {
		params.lelimit = Math.min(500, remaining);

		let data = await client.request(params);


		let pagesRetrieved = 0;
		data.query.logevents.forEach((e) => {
			if (e.hasOwnProperty('user')) {

				const userName = e.user;
				summary[userName] = (summary[userName] || 0) + 1;

				pagesRetrieved++;
			}
		});

		remaining -= pagesRetrieved;

		// set new params
		delete params.lecontinue;
		delete params.rvcontinue;


		if (!data.hasOwnProperty('continue')) {
			break;
		} else { // more than 12 MB
			if (data.continue.hasOwnProperty('rvcontinue')) { // ?
				params.rvcontinue = data.continue.rvcontinue;
			} else if (data.continue.hasOwnProperty('lecontinue')) {
				params.lecontinue = data.continue.lecontinue;
			}
		}
	}

	return summary;
}


module.exports = router;
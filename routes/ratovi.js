const express = require('express')

const { mwn } = require('mwn');
const { default: title } = require('mwn/build/title');

var init = require('../static/init.js');

var https = require('https');


const router = express.Router()

router.get("/", async (req, res) => {

	await generateEditWarsPage(req, res);
	res.end();
});

async function generateEditWarsPage(req, res) {
	// bot account and database access credentials, if needed
	const crendentials = require('../../credentials.json');

	const client = await mwn.init({
		apiUrl: 'https://hr.wikipedia.org/w/api.php',
		// Can be skipped if the bot doesn't need to sign in    
		username: crendentials.bot_username,
		password: crendentials.bot_password,

		// Set your user agent (required for WMF wikis, see https://meta.wikimedia.org/wiki/User-Agent_policy):    
		userAgent: init.userAgent,
		// Set default parameters to be sent to be included in every API request    
		defaultParams: {
			assert: 'user' // ensure we're logged in    
		}
	});

	// need to do either a .getSiteInfo() or .login() before we can use the client object  
	await client.getSiteInfo();
	//await client.login();

	console.log(`/ratovi:`)

	res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
	res.write(init.generateHead("ratovi izmjenama", 0, 1));

	res.write(init.nav);

	res.write('<div class="container" style="padding-top: 10px;">')

	let list = await getRC(req, res, client, 2000);
	let summary = await identifyReverts(list);

	let max = list[0];
	let min = list[list.length-1];


	const sortedSummary = summary
		.filter(entry => (entry.revAuto.size + entry.revManual.size) >= 1)
		.sort((a, b) => (b.revAuto.size + b.revManual.size) - (a.revAuto.size + a.revManual.size));

	printOutput(req, res, sortedSummary, min, max);
}


async function getRC(req, res, client, limit) {
	let list = [];

	let remaining = limit;
	let params = {
		"action": "query",
		"format": "json",
		"list": "recentchanges",
		"formatversion": "2",
		"rcdir": "older",
		"rcnamespace": "0",
		"rcprop": "ids|timestamp|title|flags|comment|user|tags|sizes",
		"rcshow": "!bot|!redirect",
		"rctype": "edit"
	}

	while (remaining > 0) {
		params.rclimit = Math.min(500, remaining);

		let data = await client.request(params);

		let pagesRetrieved = 0;
		data.query.recentchanges.forEach((e) => {
			if (e.hasOwnProperty('title')) {
				pagesRetrieved++;
				list.push(e);
			}
		});

		remaining -= pagesRetrieved;
		console.log(remaining + ' pages left')

		// set new params
		delete params.rccontinue;
		delete params.rvcontinue;


		if (!data.hasOwnProperty('continue')) {
			break;
		} else { // more than 12 MB
			if (data.continue.hasOwnProperty('rvcontinue')) {
				params.rvcontinue = data.continue.rvcontinue; //?
			} else if (data.continue.hasOwnProperty('rccontinue')) {
				params.rccontinue = data.continue.rccontinue;
			}
		}
	}

	// analiza izmjena

	return list;
}

async function identifyReverts(list) {
	const summary = [];

	for (const e of list) {
		const title = e.title;

		// Find the entry in the array or create a new one
		let entry = summary.find(item => item.title === title);
		if (!entry) {
			entry = { title, total: 0, revAuto: new Set(), revManual: new Set() };
			summary.push(entry);
		}

		entry.total += 1;

		if (!entry.hasOwnProperty("revids")) {
			entry.revids = new Set();
		}
		entry.revids.add(e.revid);

		if (e.tags.includes("mw-undo") || e.tags.includes("mw-rollback")) {
			entry.revAuto.add(e.revid);
		} else {

			if (/(?:undo|undid|revert|uklon|uklanja)/i.test(e.comment)) {
				entry.revManual.add(e.revid);
			}
		}
	}
	return summary;
}

async function printOutput(req, res, summary, min, max) {
	let colorManual = '#ffcc66';
	let colorAuto = 'yellow';
	let lang = 'hr';

	res.write('Brza uklanjanja označena su <span style="background-color: ' + colorAuto + ';">ovako</span>. Ručna uklanjanja označena su <span style="background-color: ' + colorManual + ';">ovako</span>.<br />');
	res.write('Raspon: ' + shortDate(init.getTranslation(lang, "monthNamesShort"), min.timestamp))
	res.write(` (<a href="https://hr.wikipedia.org/wiki/Special:Diff/${min.revid}" target="_blank">${min.revid}</a>)`);
	res.write( ' -- ' + shortDate(init.getTranslation(lang, "monthNamesShort"), max.timestamp));
	res.write(` (<a href="https://hr.wikipedia.org/wiki/Special:Diff/${max.revid}" target="_blank">${max.revid}</a>)`);
	res.write('<ol>');
	for (const e of summary) {
		const sortedRevids = Array.from(e.revids).sort((a, b) => a - b); //lowest first
		const revidsLinks = sortedRevids.map(revid => {
			const color = e.revAuto.has(revid) ? colorAuto : (e.revManual.has(revid) ? colorManual : 'none');
			return `<a href="https://hr.wikipedia.org/wiki/Special:Diff/${revid}" style="background-color: ${color};" target="_blank">${revid}</a>`;
		}).join(', ');

		res.write(`<li>${e.title}: ${e.total} izmjena (${e.revAuto.size} brza uklanjanja, ${e.revManual.size} ručno) - (${revidsLinks})</li>`);
	}

	res.write('</ol>');
}
function shortDate(monthNames, datumRaw) {
    var datum = new Date(datumRaw);
    
    // Set the timezone to "Europe/Zagreb"
    var options = { timeZone: 'Europe/Zagreb' };
    
    // Format the date string
    var dateString = datum.toLocaleString('hr-HR', options);
    
    return dateString;
}

module.exports = router;
const express = require('express')

const { mwn } = require('mwn');
const { default: title } = require('mwn/build/title');

var init = require('../static/init.js');

const router = express.Router()

const naslov = "Pretraga parametra predloška";
let lang = undefined;
let nsLocalName = undefined;
let gtilimit = undefined;
let paramInput = undefined;
let nsInput = undefined;

router.get("/:lang", async (req, res) => {
	nsLocalName = undefined;
	gtilimit = undefined;
	paramInput = undefined;
	nsInput = undefined;

	var langLoc = req.params.lang;


	if (!init.wikiLangs.includes(langLoc)) {
		// krivo ime
		res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
		res.write(init.generateErrorPage('Template analysis', 'Unsupported language.'));
		console.log('/predlozak2 (start) - lang fail: ' + langLoc);
		res.end();
		return;
	} else {
		lang = langLoc;
	}

	if (req.query.tl) {

		var predlozak = req.query.tl;
		predlozak = predlozak.charAt(0).toUpperCase() + predlozak.slice(1) //ne toLowerCase -  Infookvir Harry Potter lik
		var limit = req.query.limit || (lang === 'hr' ? "500" : "100");

		// ako nijedno nije null
		if (!predlozak.match(regexp = "^[A-Za-z0-9ČčĆćĐđŠšŽž.,:_ -]+$")) {
			console.log('/predlozak2/' + lang + ' - predlozak fail: ' + predlozak);
			// krivo ime
			res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
			res.write(init.generateErrorPage(naslov, 'Template name contains unsupported characters.'));
			res.end();
			return;
		}

		if (req.query.param) {
			paramInput = req.query.param || undefined;

			if (!paramInput.match(regexp = "^[A-Za-z0-9ČčĆćĐđŠšŽž_ -]+$")) {
				console.log('param fail: ' + paramInput);
				// krivo ime
				res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
				res.write(init.generateErrorPage(naslov, 'Parametar sadrži nedopuštene znakove.'));
				res.end();
				return;
			}
		}

		if (req.query.ns) {
			nsInput = req.query.ns || undefined;

			if (!nsInput.match(regexp = "^[0-9]{1,3}$")) {
				console.log('namespace fail: ' + nsInput);
				// krivi ns
				res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
				res.write(init.generateErrorPage(naslov, 'Incorrect namespace.'));
				res.end();
				return;
			}
		}

		if (limit.match(regexp = "^[1-9][0-9]*$") && !isNaN(limit) && limit >= 1 && limit <= 5000) {
			try {
				await generateResultPage(req, res, predlozak, paramInput, nsInput, limit);
			}
			catch (err) {
				init.failureCallback(err, req, res);
			}

		} else {
			console.log('/predlozak2/' + lang + ' - limit fail: ' + limit);
			res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
			res.write(init.generateErrorPage(naslov, 'Incorrect limit set (1 <= limit <= 5000).'));
			res.end();
			return;
		}

	} else {
		// forma
		res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
		res.write(init.generateHead(naslov));

		var langLoc = req.params.lang;

		if (!init.wikiLangs.includes(langLoc)) {
			console.log('/predlozak2 (forma) - lang fail: ' + langLoc);
			// krivo ime
			res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
			res.write(init.generateErrorPage('Template analysis', 'Unsupported language.'));
			res.end();
			return;
		} else {
			lang = langLoc;
		}

		res.write(init.nav);

		var form = `<div class="container" style="padding-top: 10px;">
		${init.getTranslation(lang, 'p-title')}<br />
		<form action="/predlozak2/${lang}" method="get">

        <label for="tl">${init.getTranslation(lang, 'p-template')}</label>
        <input type="text" id="tl" name="tl" placeholder="${init.getTranslation(lang, 'p-example')}"/> <span> (${init.getTranslation(lang, 'p-noprefix')})</span>
		<br />
		<label for="param">${init.getTranslation(lang, 'p-param')}</label>
		<input type="text" id="param" name="param" placeholder="${init.getTranslation(lang, 'p-param-example')}"/> <span> (${init.getTranslation(lang, 'p-param-empty')})</span>
		<br />
		<label for="ns">${init.getTranslation(lang, 'p-ns')}</label>
		<input type="number" id="ns" name="ns" min="0" value="0"/> <span> (${init.getTranslation(lang, 'p-ns-suggestions')})</span>
		<br />
        <input type="submit" value="${init.getTranslation(lang, 'p-search')}"></input>
        </form>`;
		res.write(form + '<br />');
		res.write(init.getTranslation(lang, 'p-warn-start') + init.colorText('red', init.getTranslation(lang, 'p-warn-veryIntensive')) + init.getTranslation(lang, 'p-warn-end'));
		res.write(`<div>${init.getTranslation(lang, 'p-limits')}</div>`)
		init.chooser(res, 'predlozak2')
	}
	res.end();
});


async function generateResultPage(req, res, template, paramInput, nsInput, limit) {
	const startMem = process.memoryUsage().rss / 1024 / 1024; //MB
	const startTime = process.hrtime();

	apiUrl = 'https://' + lang + '.wikipedia.org';

	// bot account and database access credentials, if needed
	const crendentials = require('../../credentials.json');

	const client = await mwn.init({
		apiUrl: apiUrl + '/w/api.php',
		// Can be skipped if the bot doesn't need to sign in    
		username: crendentials.bot_username,
		password: crendentials.bot_password,

		// Set your user agent (required for WMF wikis, see https://meta.wikimedia.org/wiki/User-Agent_policy):    
		userAgent: init.userAgent,
		// Set default parameters to be sent to be included in every API request    
		defaultParams: {
			assert: 'user' // ensure we're logged in    
		}
	});

	// need to do either a .getSiteInfo() or .login() before we can use the client object  
	await client.getSiteInfo();
	//await client.login();

	console.log('/predlozak2/' + lang + ': ', template + '; param: ' + (paramInput || '(all)') + '; ns ' + (nsInput || '0') + '; limit:  ' + limit);

	res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
	res.write(init.generateHead(naslov));

	res.write(init.nav);

	res.write('<div class="container" style="padding-top: 10px;">');

	nsLocalName = await getTemplateLocalName(client);


	const content = await getTemplateContent(template, client);
	if (content.glavnoIme.length == 0 && content.content.length == 0) {
		res.write(init.colorText('red', 'Entered template doesn\'t exist.'));
		console.log(lang + ': Predložak ' + template + ' ne postoji!');
		return 0;
	}

	const startingTemplateParamsArr = getParams(content.content);
	var templateParamsArr = [];

	if (paramInput) { //if searching by single param, remove all others
		if (startingTemplateParamsArr.findIndex(e => e.toLocaleLowerCase() == paramInput.toLocaleLowerCase()) != -1) {
			templateParamsArr = startingTemplateParamsArr.filter(e => e.toLocaleLowerCase() == paramInput.toLocaleLowerCase())
		} else { // if parameter doesn't exist
			res.write(init.colorText('red', 'Entered param does not exist within template.'));
			console.log(lang + ': Predložak ' + template + ' - parametar  ' + paramInput + ' ne postoji!');
			return 0;
		}
	} else {
		templateParamsArr = startingTemplateParamsArr;
	}

	const redir = await getRedirects(template, client);
	template = redir.glavnoIme;


	const fullArticleList = await dohvatiPopisISadrzaj(template, limit, nsInput, client);
	let origLen = fullArticleList.length;

	if (fullArticleList.length == 0) {
		// template not used in any articles
		res.write(init.colorText('red', 'Template exists but is not used on any pages.'));
		console.log(lang + ': Predložak ' + template + ' nije korišten!');
		return 0;
	}

	// ignore /doc subpages and errors in revisions
	let articleList = fullArticleList.filter(a => (!a.title.startsWith(redir.glavnoIme)) && a.revisions.length === 1);
	let articlesRejected = origLen - articleList.length;

	const link = redir.glavnoIme.split(":")[1];

	if (paramInput) {
		res.write(
			init.getTranslation(lang, 'p-header-single',
				'<a href="https://' + lang + '.wikipedia.org/wiki/' + nsLocalName + link.replace(' ', '_') + '">' + link + '</a>',
				templateParamsArr[0], articleList.length));

	} else {
		res.write(
			init.getTranslation(lang, 'p-header',
				'<a href="https://' + lang + '.wikipedia.org/wiki/' + nsLocalName + link.replace(' ', '_') + '">' + link + '</a>',
				templateParamsArr.length, articleList.length));
	}


	res.write(':<br />');
	if (articleList.length === parseInt(limit)) {
		res.write(init.colorText('red', init.getTranslation(lang, 'p-warn-limit', limit)))
	}

	//TODO lista sort
	if (!paramInput) {
		res.write('<div>' + init.getTranslation(lang, 'p-jump-unused') + '</div><br />')
	}

	redir.preusmj.push(redir.glavnoIme);


	res.write('<div class="accordion" id="accordionExample">');
	const { accordionArr, unusedParams, notFoundByArticle, notFoundByParam, parseFail } = processParamsInArticles(redir, templateParamsArr, articleList, client, res);
	res.write(accordionArr.join(''));
	res.write('</div>');

	if (parseFail.size > 0) {
		let out = []
		out.push('<br />' + init.colorText('red', init.getTranslation(lang, 'p-parsing-failed'), 1) +
			init.getTranslation(lang, 'p-parsing-failed-desc'));
		out.push('<ol style ="-webkit-column-count: 3; -moz-column-count: 3; column-count: 3;">');
		parseFail.forEach(item => {
			out.push(`<li><a href="https://${lang}.wikipedia.org/wiki/${item.replace(' ', '_')}">${item}</a></li>`)
		})
		out.push('</ol>');
		res.write(out.join(''));

	}

	if (notFoundByArticle.length > 0) {

		notFoundByArticle.sort((a, b) => b.params.length - a.params.length);

		notFoundByParam.sort((a, b) => {
			if (b.values.length !== a.values.length) {
				return b.values.length - a.values.length;
			} else {
				const aValuesLength = a.values.reduce((acc, val) => acc + val.value.length, 0);
				const bValuesLength = b.values.reduce((acc, val) => acc + val.value.length, 0);
				return bValuesLength - aValuesLength;
			}
		});

		const htmlContent = `
			</div>
			<br /><br />
			<div class="flex-container" style="margin: 0 20px;">
				<div style="text-align:center;">${init.colorText('red', init.getTranslation(lang, 'p-not-found-notice-top'), 0)}</div>
				<div class="row">
					<div class="col-md-1 slot-left"></div>
					<div class="col-md-7 slot-left">
						<div style="text-align:center;">${init.colorText('darkslateblue', init.getTranslation(lang, 'p-not-found-grouped-article'), 0)}</div>
						<ol>${PrintNotFoundByArticle(notFoundByArticle).join('')}</ol>
					</div>
					<div class="col-md-4 slot-right">
						<div style="text-align:center;">${init.colorText('darkslateblue', init.getTranslation(lang, 'p-not-found-grouped-param'), 0)}</div>
						<ol>${PrintNotFoundByParam(notFoundByParam).join('')}</ol>
					</div>
				</div>
			</div>`;

		res.write(htmlContent);
	}

	res.write('</div>');
	res.write('</div>');

	res.write('</div>'); // flex-container-end
	res.write('<div class="container">')

	if (!paramInput) {
		res.write(printDocsStruct(redir, templateParamsArr, unusedParams).join(''));
	}

	const endTime = process.hrtime(startTime);
	const elapsedTimeInSeconds = (endTime[0] + endTime[1] / 1e9);
	const memoryUsage = (process.memoryUsage().rss / 1024 / 1024) - startMem;


	res.write(init.getTranslation(lang, 'p-final-time', elapsedTimeInSeconds.toFixed(3), memoryUsage.toFixed(3),));

	if (!paramInput) {
		res.write(' (' +
			init.getTranslation(lang, 'p-final-time-partials', (elapsedTimeInSeconds / templateParamsArr.length).toFixed(3),
				(memoryUsage / templateParamsArr.length).toFixed(3)) + ')');
	}
	res.end();
}


function getParams(content) {
	const paramNames = [];
	const paramRegex = /{{{([\s\S]*?)(\||})/g;

	let match;
	while ((match = paramRegex.exec(content)) !== null) {
		const paramName = match[1].trim();
		if (!paramNames.includes(paramName)) {
			paramNames.push(paramName);
		}
	}

	return paramNames;
}

async function getTemplateLocalName(client) {
	let ns = ''
	await client.request({
		"action": "query",
		"format": "json",
		"meta": "siteinfo|userinfo",
		"formatversion": "2",
		"siprop": "namespaces",
		"uiprop": "rights"
	}).then(async function (data) {
		ns = data.query.namespaces[10].name + ':';
		gtilimit = (data.query.userinfo.rights.includes('apihighlimits')) ? 500 : 50;
		//console.log('gtilimit: ' + gtilimit)
	});

	return ns;
}

async function getTemplateContent(predlozak, client) {
	let glavnoIme = ''
	let content = ''
	await client.request({
		"action": "query",
		"format": "json",
		"prop": "revisions",
		"titles": nsLocalName + predlozak,
		"redirects": 1,
		"formatversion": "2",
		"rvprop": "content",
		"rvslots": "main"
	}).then(async function (data) {

		if (!data.query.pages[0].hasOwnProperty('missing')) {
			glavnoIme = data.query.pages[0].title;
			content = data.query.pages[0].revisions[0].slots.main.content;
		}
	});

	return { glavnoIme, content }
}

async function getRedirects(predlozak, client) {
	let glavnoIme = ''
	let preusmj = []

	await client.request({
		"action": "query",
		"format": "json",
		"prop": "redirects",
		"titles": nsLocalName + predlozak,
		"redirects": 1,
		"formatversion": "2",
		"rdprop": "pageid|title"
	}).then(async function (data) {
		if (data.query.redirects) {
			glavnoIme = data.query.redirects[0].to;
		} else {
			glavnoIme = nsLocalName + predlozak;
		}

		if (data.query.pages[0].redirects) {
			preusmj = data.query.pages[0].redirects.map(redirect => redirect.title);
		}
	});

	return { glavnoIme, preusmj };

}

async function dohvatiPopisISadrzaj(predlozak, limit, namespace, client) {
	let lista = [];
	let remaining = limit;
	let params = {
		"action": "query",
		"format": "json",
		"prop": "revisions",
		"titles": predlozak,
		"generator": "transcludedin",
		"formatversion": "2",
		"rvprop": "content",
		"rvslots": "main",
		"gtiprop": "pageid|title|redirect",
	};
	if (namespace) {
		params.gtinamespace = namespace;
	}

	while (remaining > 0) {
		params.gtilimit = Math.min(gtilimit, remaining);
		let data = await client.request(params);

		// template not used
		if (!data.query) return [];

		let pagesRetrieved = 0;
		data.query.pages.forEach((e) => {
			if (e.hasOwnProperty('revisions')) {
				lista.push(e);
				pagesRetrieved++;
			}
		});

		remaining -= pagesRetrieved;
		console.log('Total results retrieved: ' + lista.length);

		// set new params
		delete params.gticontinue;
		delete params.rvcontinue;


		if (!data.hasOwnProperty('continue')) {
			break;
		} else { // more than 12 MB
			if (data.continue.hasOwnProperty('rvcontinue')) {
				params.rvcontinue = data.continue.rvcontinue;
			} else if (data.continue.hasOwnProperty('gticontinue')) {
				params.gticontinue = data.continue.gticontinue;
			}

		}
	}

	return lista;
}

function processParamsInArticles(redir, templateParamsArr, filteredArticleList, client, res) {
	const tParamSet = new Set(templateParamsArr);
	const redirSet = new Set(redir.preusmj.map(item => item.split(":")[1]));
	const aTemplatesMap = new Map();

	let unusedParams = [];
	let parseFail = new Set();
	let accordionArr = [];
	let runSecondLoop = true;
	let notFoundByArticle = []; // { article, param[{name, value}] }
	let notFoundByParam = []; // { param.name, [{article, value}] }

	// Helper to handle template parsing
	const parseTemplatesForArticle = (article) => {
		if (!aTemplatesMap.has(article.title)) {
			const content = article.revisions[0].slots.main.content.removeComments();
			const templates = new client.wikitext(content).parseTemplates()
				.filter(tpl => redirSet.has(tpl.name.replace('_', ' ')));

			if (templates.length === 0 && runSecondLoop) {
				parseFail.add(article.title);
			}
			aTemplatesMap.set(article.title, templates);
		}
		return aTemplatesMap.get(article.title);
	};

	// Helper to process template parameters and track missing params
	const processTemplateParams = (article, param, hasParam, emptyParam, noParam) => {
		const templates = parseTemplatesForArticle(article);

		templates.forEach(aTemplate => {
			const paramValue = aTemplate.getParam(param)?.value.trim();
			if (paramValue === undefined) {
				noParam.push(article.title);
			} else if (paramValue === '') {
				emptyParam.push(article.title);
			} else {
				hasParam.push(article.title);
			}

			if (!runSecondLoop) return;

			aTemplate.parameters.forEach(aParam => {
				try {
					const paramName = aParam.name.toString();
					if (!tParamSet.has(paramName)) {
						// Update notFoundByArticle
						let articleEntry = notFoundByArticle.find(entry => entry.title === article.title);
						if (!articleEntry) {
							articleEntry = { title: article.title, params: [] };
							notFoundByArticle.push(articleEntry);
						}
						articleEntry.params.push({ name: paramName, value: aParam.value });

						// Update notFoundByParam
						let paramEntry = notFoundByParam.find(entry => entry.param === paramName);
						if (!paramEntry) {
							paramEntry = { param: paramName, values: [] };
							notFoundByParam.push(paramEntry);
						}
						paramEntry.values.push({ article: article.title, value: aParam.value });
					}
				} catch (error) {
					logError(article, aParam, error);
				}
			});
		});
	};

	// Helper to handle error messages
	const logError = (article, aParam, error) => {
		res.write(init.colorText('red', `Error processing article <b>${article.title}</b>: ${JSON.stringify(aParam)}`));
		console.error(`Error processing article ${article.title}: ${JSON.stringify(aParam)}`);
	};

	// Main processing loop
	templateParamsArr.forEach((param, index) => {
		let hasParam = [];
		let emptyParam = [];
		let noParam = [];

		filteredArticleList.forEach(article => {
			processTemplateParams(article, param, hasParam, emptyParam, noParam);
		});

		runSecondLoop = false; // Disable second loop after processing all articles

		if (hasParam.length === 0) {
			unusedParams.push(param);
		}

		const title = init.getTranslation(lang, 'p-acc-head', param, hasParam.length, emptyParam.length, noParam.length);
		const content = PrintAccordionContent(hasParam, emptyParam, noParam, param, lang);
		accordionArr.push(PrintAccordionStruct(index, title, content));
	});

	return { accordionArr, unusedParams, notFoundByArticle, notFoundByParam, parseFail };
}

String.prototype.markComments = function () {
	return this.replace(/<!--[\s\S]*?-->/g, '<i>comment</i>').trim();
}

String.prototype.removeComments = function () {
	return this.replace(/<!--[\s\S]*?-->/g, '').trim();
}

function PrintAccordionStruct(index, title, content) {
	var classContent = 'collapse';

	if (paramInput) {
		classContent += ' show'
	}

	return html = `
	<div class="accordion-item">
  <h2 class="accordion-header">
    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse${index}"
      aria-expanded="true" aria-controls="collapse${index}">
	  	<span class="d-block w-100 text-center" style="font-size:1rem;">${title}</span>
    </button>
  </h2>
  <div id="collapse${index}" class="accordion-collapse ${classContent}" data-bs-parent="#accordionExample">
    <div class="accordion-body">
      ${content}
    </div>
  </div>
</div>
 
  `

}

function PrintAccordionContent(hasParam, emptyParam, noParam, param) {
	const style = 'width:33%; display: inline-block; vertical-align: top;';
	param = `<b>${param}</b>`;

	const generateList = (list) => {
		if (list.length > 0) {
			return `<ol>${list
				.map(item => `<li><a href="https://${lang}.wikipedia.org/wiki/${item.replace(' ', '_')}">${item}</a></li>`)
				.join('')}</ol>`;
		} else {
			return 'Nema';
		}
	};

	const generateSection = (title, list, color) => {
		const content = generateList(list);
		return `<div style="${style}">
                    <h2>
					${init.getTranslation(lang, 'p-acc-content', color, title, param)}</h2>
                    ${content}
                </div>`;
	};

	const hasParamContent = generateSection(init.getTranslation(lang, 'p-acc-content-have'), hasParam, 'green');
	const emptyParamContent = generateSection(init.getTranslation(lang, 'p-acc-content-empty'), emptyParam, 'purple');
	const noParamContent = generateSection(init.getTranslation(lang, 'p-acc-content-none'), noParam, 'red');

	return `<div style="display: inline-block; width: 100%;">${hasParamContent}${emptyParamContent}${noParamContent}</div>`;
}

function PrintNotFoundByArticle(notFoundByArticle) {
	let output = [];

	notFoundByArticle.forEach(e => {
		let paramsHTML = e.params.map(param => {
			let trimmedValue = param.value.trim();
			if (trimmedValue.length === 0) {
				// If value is empty after trimming, color it purple
				return `<span style="color: purple;">${param.name}</span>`;
			} else {
				// If value is not empty after trimming, color it green
				return `<b style="color: green;">${param.name}</b> (${trimmedValue})`;
			}
		}).join(', ');
		let articleLink = `https://${lang}.wikipedia.org/wiki/${e.title.replace(' ', '_')}`;
		output.push(`<li><a href="${articleLink}">${e.title}</a>: ${paramsHTML}</li>`);
	});

	return output;
}

function PrintNotFoundByParam(notFoundByParam) {
	let output = [];

	for (const entry of notFoundByParam) {
		let emptyValueCount = 0;
		let nonEmptyValueCount = 0;

		for (const valueEntry of entry.values) {
			if (valueEntry.value.trim().length === 0) {
				emptyValueCount++;
			} else {
				nonEmptyValueCount++;
			}
		}

		output.push(`<li><b>${entry.param}</b>`);
		if (nonEmptyValueCount || emptyValueCount) {
			output.push(`: `);
			if (nonEmptyValueCount) {
				output.push(`<b style="color: green;">${nonEmptyValueCount}</b> ${init.getTranslation(lang, 'p-grouped-param-used')}`);
				if (emptyValueCount) {
					output.push(`; `);
				}
			}
			if (emptyValueCount) {
				output.push(`<span style="color: purple;">${emptyValueCount}</span> ${init.getTranslation(lang, 'p-grouped-param-unused')}`);
			}
			if (nonEmptyValueCount && emptyValueCount) {
				output.push(` (${nonEmptyValueCount + emptyValueCount} ${init.getTranslation(lang, 'p-grouped-param-total')})`);
			}
		} else {
			output.push(`: No data available`);
		}
		output.push(`</li>`);
	}

	return output;
}


function printNekoristeniParametri(array) {
	let output = []
	output.push(init.getTranslation(lang, 'p-unused-desc', init.colorText('green', init.getTranslation(lang, 'p-unused-contains'))))
	if (array.length == 0) {
		output.push('<br/>' + init.getTranslation(lang, 'p-no-unused'));
	} else {
		output.push('<ol>');

		array.forEach(item => {
			output.push('<li>' + item + '</li>');
		});

		output.push('</ol>');
	}
	return output;
}

function printDocsStruct(redir, templateParamsArr, unusedParams) {

	let style = 'width:33%; display: inline-block; vertical-align: top;'
	let docsOut = []
	docsOut.push('<div id="nekoristeni" style="padding-top:50px;">')
	docsOut.push('<div style=" ' + style + '">');
	docsOut.push(printNekoristeniParametri(unusedParams).join(''));
	docsOut.push('</div>');

	docsOut.push('<div style=" ' + style + '">');
	docsOut.push(printDocs(redir.glavnoIme, templateParamsArr, unusedParams, 1).join(''));
	docsOut.push('</div>');

	if (unusedParams.length > 0) {
		docsOut.push('<div style=" ' + style + '">');
		docsOut.push(printDocs(redir.glavnoIme, templateParamsArr, unusedParams, 0).join(''));
		docsOut.push('</div>');
	}
	docsOut.push('</div>');
	return docsOut;
}

function printDocs(imePredloska, parametriArr, nekoristeniParam, isFull = 1) {
	imePredloska = imePredloska.split(":")[1];
	if (isFull == 0) {
		parametriArr = parametriArr.filter(item => !nekoristeniParam.includes(item));
	}

	let output = ['<div style="margin-bottom:10px;">'];
	if (isFull == 0) {
		output.push(init.getTranslation(lang, 'p-docs-used'));
	} else {
		output.push(init.getTranslation(lang, 'p-docs-full'));
	}
	output.push('</div>')
	output.push('<pre>');
	output.push('{{' + imePredloska + '<br>');

	// Find the length of the longest parameter
	let maxParamLength = 0;
	for (const param of parametriArr) {
		if (param.length > maxParamLength) {
			maxParamLength = param.length;
		}
	}
	maxParamLength++;

	// Print each parameter with dynamically calculated spaces
	for (const param of parametriArr) {
		const spaces = ' '.repeat(maxParamLength - param.length);
		output.push('| ' + param + spaces + ' = <br />');
	}

	output.push('}}</pre>');

	return output;
}

module.exports = router;
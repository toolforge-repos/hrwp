const express = require('express')

const { mwn } = require('mwn');
const { default: title } = require('mwn/build/title');

var init = require('../static/init.js');
const { forEach } = require('xregexp');

const router = express.Router()

const naslov = "Provjera kategorija";

router.get("/", async (req, res) => {

	if (req.query.cat) {

		var catLang = req.query.catlang;
		var catName = req.query.cat;
		var targetLang = req.query.targetlang;

		var limit = req.query.ogr || "500";

		var reverseParam = req.query.r;

		// ako nijedno nije null
		if (!catName.match(regexp = "^[A-Za-z0-9ČčĆćĐđŠšŽž.,:_ -]+$")) {
			console.log('/catcheck - cat fail: ' + catName);
			// krivo ime
			res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
			res.write(init.generateErrorPage(naslov, 'Category name contains invalid characters.'));
			res.end();
			return;
		}

		if (!init.wikiLangs.includes(catLang)) {
			console.log('/catcheck - catlang fail: ' + catLang);
			// krivo ime
			res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
			res.write(init.generateErrorPage(naslov, 'Category language is invalid.'));
			res.end();
			return;
		}

		if (!init.wikiLangs.includes(targetLang)) {
			console.log('/catcheck - targetlang fail: ' + targetLang);
			// krivo ime
			res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
			res.write(init.generateErrorPage(naslov, 'Target language is invalid.'));
			res.end();
			return;
		}

		if (limit.match(regexp = "^[1-9][0-9]*$") && !isNaN(limit) && limit >= 1 && limit <= 5000) {

			if (reverseParam) {
				console.log(`/catcheck reverse: ${catLang}:Category:${catName} into ${targetLang}`);
				let redirectUrl = await reverseLangs(catLang, catName, targetLang);
				if (redirectUrl.length > 5) {
					console.log('/catcheck redirecting: ' + redirectUrl)
					res.redirect(redirectUrl);
				} else {
					console.log(`/catcheck reverse fail: ${catLang}:Category:${catName} does not exist on ${targetLang}`);
					res.write(init.generateErrorPage(naslov, `Equivalent to :${catLang}:Category:${catName} does not exist on ${targetLang}wiki! Check interwikis.`));
					res.end();
					return;
				}
			}
			else {
				await generateResultPage(req, res, catLang, catName, targetLang, limit);
			}
		} else {
			console.log('/catcheck - limit fail: ' + limit);
			res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
			res.write(init.generateErrorPage(naslov, 'Limit is invalid. Limit must be a number in range 1 ≤ number ≤ 5000'));
			res.end();
			return;
		}

	} else {
		// forma
		res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
		res.write(init.generateHead(naslov));

		res.write(init.nav);

		res.write('<div class="container" style="padding-top: 10px;">')
		res.write('<p>Enter an initial language, the name of a category in that language, and a destination language.<br>This page will list all articles from the initial category present in the destination language:</p>');

		var form = `<form action="/catcheck" method="get">

		<label for="catlang">Language:</label>
        <input type="text" id="catlang" name="catlang" placeholder="en" required style="width:3em;"/>
        <label for="cat">Category:</label>
        <input type="text" id="cat" name="cat" placeholder="e.g. Physics" required/> <span> (no "<i>Category:</i>" prefix)</span>
        <br />
		<label for="targetlang">Destination language:</label>
        <input type="text" id="targetlang" name="targetlang" placeholder="hr" required style="width:3em;"/>
		<br />
        <input type="submit" value="Search"></input>
        </form>`;
		res.write(form);
	}
	res.end();
});


async function reverseLangs(catLang, catName, targetLang) {

	const crendentials = require('../../credentials.json');
	const client1 = await mwn.init({
		apiUrl: `https://${catLang}.wikipedia.org/w/api.php`,
		// Can be skipped if the bot doesn't need to sign in    
		username: crendentials.bot_username,
		password: crendentials.bot_password,

		// Set your user agent (required for WMF wikis, see https://meta.wikimedia.org/wiki/User-Agent_policy):    
		userAgent: init.userAgent,
		// Set default parameters to be sent to be included in every API request    
		defaultParams: {
			assert: 'user' // ensure we're logged in    
		}
	});

	await client1.getSiteInfo();


	let reverseCatName = await getReverseCatName(client1, catLang, catName, targetLang);
	if (!reverseCatName) {
		return '';
	} else {
		reverseCatName = reverseCatName.replace(/^.*?:/, '');
		return `/catcheck?catlang=${targetLang}&cat=${reverseCatName}&targetlang=${catLang}`;
	}

}

async function generateResultPage(req, res, catLang, catName, targetLang, limit) {
	apiUrl = `https://${catLang}.wikipedia.org`;

	// bot account and database access credentials, if needed
	const crendentials = require('../../credentials.json');

	const strings = require("../static/strings.json");

	const client1 = await mwn.init({
		apiUrl: `https://${catLang}.wikipedia.org/w/api.php`,
		// Can be skipped if the bot doesn't need to sign in    
		username: crendentials.bot_username,
		password: crendentials.bot_password,

		// Set your user agent (required for WMF wikis, see https://meta.wikimedia.org/wiki/User-Agent_policy):    
		userAgent: init.userAgent,
		// Set default parameters to be sent to be included in every API request    
		defaultParams: {
			assert: 'user' // ensure we're logged in    
		}
	});

	// need to do either a .getSiteInfo() or .login() before we can use the client1 object  
	await client1.getSiteInfo();
	//await client1.login();


	//2nd
	const client2 = await mwn.init({
		apiUrl: `https://${targetLang}.wikipedia.org/w/api.php`,
		// Can be skipped if the bot doesn't need to sign in    
		username: crendentials.bot_username,
		password: crendentials.bot_password,

		// Set your user agent (required for WMF wikis, see https://meta.wikimedia.org/wiki/User-Agent_policy):    
		userAgent: init.userAgent,
		// Set default parameters to be sent to be included in every API request    
		defaultParams: {
			assert: 'user' // ensure we're logged in    
		}
	});


	// need to do either a .getSiteInfo() or .login() before we can use the client1 object  
	await client2.getSiteInfo();
	//await client1.login();

	//both clients init

	console.log('/catcheck: ' + catLang + ':' + catName + ' into ' + targetLang + '; limit: ' + limit)

	res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
	res.write(init.generateHead(naslov + ' - ' + catLang + ':' + catName + ' into ' + targetLang));

	res.write(init.nav);

	const reverseCatName = await getReverseCatName(client1, catLang, catName, targetLang);

	res.write(`<span class="center">Searching for members of <a href="https://${catLang}.wikipedia.org/wiki/Category:${catName.replaceAll(' ', '_')}">
			:${catLang}:Category:${catName}</a> on <i>${targetLang}wiki</i>:`);
	if (reverseCatName.length > 0) {
		res.write(`<br>&nbsp;Category exists as 
			<a href="https://${targetLang}.wikipedia.org/wiki/${reverseCatName.replaceAll(' ', '_')}">
			:${targetLang}:${reverseCatName}</a>
			${genSearchButton(targetLang, reverseCatName.replace(/^.*?:/, ''), catLang)}`)
	} else {
		res.write(`<br>&nbsp;${init.colorText('red', 'No equivalent category exists')} on <i>${targetLang}wiki</i>!`)
	}
	res.write(`<br>Coloured categories and articles indicate the article is present in that category on ${targetLang}wiki.`)
	res.write(`<br>A checkmark next to an article indicates the article is present in the requested category on both wikis.`)
	res.write(`<br>Hover over an underlined item for more details.`)
	res.write(`</span>`)


	res.write('<div class="container-fluid" style="padding-top: 10px;">')

	const { iwitems: iwsubcats, noiwitems: noiwsubcats } = await getIWLinksforCatMembers(client1, catLang, catName, targetLang, limit, true);
	const { iwitems: iwarticles, noiwitems: noiwarticles } = await getIWLinksforCatMembers(client1, catLang, catName, targetLang, limit, false);

	if (iwsubcats.length == 0 && noiwsubcats.length == 0 && iwarticles.length == 0 && noiwarticles.length == 0) {
		console.log('/catcheck - no cat exists: ' + catLang + ':' + catName);
		res.write(init.colorText('red', catLang + ':Category:' + catName + ' does not exist.'));
		res.end();
		return;
	}

	const sourceArticleNames = iwarticles.concat(iwsubcats)
		.map(item => item.title)
		.filter(title => title !== undefined);

	const targetArticleNames = iwarticles.concat(iwsubcats)
		.map(item => item.langlinks?.[0]?.title)
		.filter(title => title !== undefined);


	let sourceCats = await getCatsForArticles(client1, sourceArticleNames, limit)
	let targetCats = await getCatsForArticles(client2, targetArticleNames, limit);

	const { crossArticles, crossCats } = await crossMatchF(iwarticles, iwsubcats, sourceCats, targetCats);

	res.write(await analyseData(catLang, catName, targetLang, reverseCatName, iwsubcats, noiwsubcats, true, crossArticles, crossCats));
	res.write(await analyseData(catLang, catName, targetLang, reverseCatName, iwarticles, noiwarticles, false, crossArticles, crossCats));

	res.write('</div>')
}

async function crossMatchF(iwarticles, iwsubcats, sourceCats, targetCats) {
	let crossArticles = [];

	/*crossArticles[] = {
		source: source,
		target: target,
		sourceCats: []
		targetCats: []
	}*/

	iwarticles.concat(iwsubcats).forEach(page => {
		crossArticles.push({
			source: page.title,
			target: page.langlinks[0].title
		})
	});

	sourceCats.forEach(e => {
		let match = crossArticles.find(k => k.source === e.title);
		if (match) match.sourceCats = e.categories.map(item => item.title);
	})

	targetCats.forEach(e => {
		let match = crossArticles.find(k => k.target === e.title);
		if (match) match.targetCats = e.categories.map(item => item.title);
	})

	let crossCats = [];

	/*crossCats[] = {
		source: catName
		target: catName
		
	}*/

	iwsubcats.forEach(subcat => {
		crossCats.push({
			source: subcat.title,
			target: subcat.langlinks[0].title,
			color: `#${Math.floor(Math.random() * 16777215).toString(16).padStart(6, '0')}`
		})
	});

	return { crossArticles, crossCats }

}

async function getReverseCatName(client1, catLang, catName, targetLang) {
	let data = await client1.request({
		"action": "query",
		"format": "json",
		"prop": "langlinks",
		"titles": "Category:" + catName,
		"redirects": 1,
		"formatversion": "2",
		"lllang": targetLang
	});

	if (data.query.pages[0].hasOwnProperty('langlinks')) {
		return data.query.pages[0].langlinks[0].title;
	} else return '';
}

async function getIWLinksforCatMembers(client1, catLang, catName, targetLang, limit, isGetSubcats) {

	let iwitems = [];
	let noiwitems = [];
	let remaining = limit;
	let params = {
		"action": "query",
		"format": "json",
		"prop": "langlinks",
		"generator": "categorymembers",
		"formatversion": "2",
		"llprop": "autonym|langname|url",
		"lllang": targetLang,
		"lllimit": "max",
		"gcmtitle": "Category:" + catName,
		"gcmlimit": "max"
	}

	isGetSubcats ? params.gcmtype = 'subcat' : params.gcmtype = 'page'

	while (remaining > 0) {
		params.gcmlimit = Math.min(500, remaining);

		let data = await client1.request(params);

		if (!data.query) {
			return { iwitems, noiwitems }
		}

		let pagesRetrieved = 0;
		data.query.pages.forEach((e) => {
			if (e.hasOwnProperty('title')) {
				if (e.hasOwnProperty('langlinks')) {
					iwitems.push(e);
				} else {
					noiwitems.push(e);
				}
				pagesRetrieved++;
			}
		});

		remaining -= pagesRetrieved;

		// set new params
		delete params.gcmcontinue;
		delete params.continue;


		if (!data.hasOwnProperty('continue')) {
			break;
		} else { // more than 12 MB
			if (data.continue.hasOwnProperty('gcmcontinue')) {
				params.continue = 'gcmcontinue||';
				params.gcmcontinue = data.continue.gcmcontinue;
			}
		}
	}

	return { iwitems, noiwitems };
}

async function analyseData(catLang, catName, targetLang, reverseCatName, iwlist, noiwlist, isSubCats, crossArticles, crossCats) {

	iwlist.sort((a, b) => {
		return a.title.localeCompare(b.title);
	});

	noiwlist.sort((a, b) => {
		return a.title.localeCompare(b.title);
	});


	let out = ['']
	if (isSubCats) {
		out.push('<b class="center">Subcats:</b>')
	} else {
		out.push('<b class="center">Pages:</b>')
	}

	out.push('<div class="row">')
	// left
	out.push('<div class="col-lg-7 col-md-12" style="padding-right:0px"><ul>')
	iwlist.forEach(item => {
		out.push('<li>')
		out.push(`<a href="https://${catLang}.wikipedia.org/wiki/${item.title.replaceAll(' ', '_')}">${item.title}</a>`);

		let sourceCats = crossArticles.find(article => article.source == item.title).sourceCats;
		if (sourceCats) {
			out.push(genSubcatsDrop(item.title, catLang, sourceCats, targetLang, catName, true))
		}

		out.push(`&nbsp;exists as <a href="https://${targetLang}.wikipedia.org/wiki/${item.langlinks[0].title.replaceAll(' ', '_')}">${item.langlinks[0].title}</a>`);

		let targetCats = crossArticles.find(article => article.source == item.title).targetCats;
		if (targetCats) {
			out.push(genSubcatsDrop(item.langlinks[0].title, targetLang, targetCats, catLang, reverseCatName, false))

			if (targetCats.includes(reverseCatName)) {
				out.push(`&nbsp;<abbr style="color:limegreen" title="Page present in category on both wikis">&#10003;</abbr>`);
			}

			//article color square of subcategory on target wiki
			if (!isSubCats) {
				const matchingTargets = crossCats
					.filter(item => targetCats.includes(item.target));

				if (matchingTargets.length > 0) {
					matchingTargets.forEach(e => {
						out.push(genColorSq(e.color,
							`Present in subcat ${e.target} on ${targetLang}wiki (translates as ${e.source})`))
					});
				}
			}
		}

		// generate all color squares and search links
		if (isSubCats) {
			let crossCatMatch = crossCats.find(cat => cat.source == item.title);
			if (crossCatMatch) {
				out.push(genColorSq(crossCatMatch.color) + '<br>' +
					'<span class="invisible-inline">Cate</span>'
					+ genSearchButton(catLang, item.title.replace(/^.*?:/, ''), targetLang)
					+ '<span class="invisible-inline"> [cats] exists as Cate</span>')
				out.push(genSearchButton(targetLang, item.langlinks[0].title.replace(/^.*?:/, ''), catLang))
			}
		}

		out.push('</li>');
	})
	out.push('</ul></div>')


	// right
	out.push('<div class="col-lg-5 col-md-12"><ul>')
	noiwlist.forEach(item => {
		out.push(`<li>
		<a href="https://${catLang}.wikipedia.org/wiki/${item.title.replaceAll(' ', '_')}">
			${item.title}
		</a> 
		does not exist`);
		(isSubCats) ? out.push(genSearchButton(catLang, item.title.replace(/^.*?:/, ''), targetLang)) : '';
		out.push('</li>');
	})
	out.push('</ul></div></div>')
	return out.join('');
}

function genSearchButton(catLang, cat, targetLang) {
	return `&nbsp;<a class="inline-button" href="catcheck?catlang=${catLang}&targetlang=${targetLang}&cat=${cat.replaceAll(' ', '_')}">[Search in ${targetLang}]</a>`
}

function genColorSq(color, text = '') {
	return `&nbsp;<span class="color-block" style="background-color: ${color};" data-toggle="tooltip" data-placement="top" title="${text}"></span>`
}

function genSubcatsDrop(article, catLang, catList, targetLang, current, stripPrefix = false) {
	let out = ['<span class="dropend">']
	out.push('<span type="button" class="inline-button" data-bs-toggle="dropdown" aria-expanded="false" style="text-decoration: underline; user-select: none;">[cats]</span>')
	out.push('<ul class="dropdown-menu max-content p-2 border-danger" style="background: lightyellow;">')
	out.push(`Categories for article <b>${catLang}:${article}</b>:`)
	catList.forEach(e => {
		if ((e === current) || (e.replace(/^.*?:/, '').replaceAll(' ', '_') === current.replaceAll(' ', '_'))) {
			
			out.push(`<li>==> <a href="https://${catLang}.wikipedia.org/wiki/${e.replaceAll(' ', '_')}">${e}</a>&nbsp;${genSearchButton(catLang, e.replace(/^.*?:/, ''), targetLang)}</li>`);
		} else {
			out.push(`<li>&nbsp;&nbsp;<a href="https://${catLang}.wikipedia.org/wiki/${e.replaceAll(' ', '_')}">${e}</a>&nbsp;${genSearchButton(catLang, e.replace(/^.*?:/, ''), targetLang)}</li>`);
		}
	})

	out.push('</ul></span>');
	return `&nbsp;` + out.join('');

}

async function getCatsForArticles(client2, iwarticles, limit) {
	let targetCats = [];
	let remaining = iwarticles.length;
	let iwarticlesRemaining = [...iwarticles]; // Clone the array to avoid mutation
	let params = {
		"action": "query",
		"format": "json",
		"prop": "categories",
		"titles": "",
		"formatversion": "2",
		"clshow": "!hidden",
		"cllimit": "max"
	};

	while (remaining > 0) {

		// Slice up to 50 articles for the current batch
		let iwarticlesCurrent = iwarticlesRemaining.slice(0, 50);
		iwarticlesRemaining = iwarticlesRemaining.slice(50); // Update remaining articles
		params.titles = iwarticlesCurrent.join('|');

		let data = await client2.request(params);

		if (!data.query) {
			return targetCats; // Return what we have if no query object
		}

		let pagesRetrieved = 0;
		data.query.pages.forEach((e) => {
			if (e.hasOwnProperty('title')) {
				if (e.hasOwnProperty('categories')) {
					targetCats.push(e);
				}
				pagesRetrieved++;
			}
		});

		remaining -= Math.min(pagesRetrieved, iwarticlesCurrent.length);

		// Reset continuation params
		delete params.clcontinue;
		delete params.continue;

		// Handle continuation for large results
		if (data.hasOwnProperty('continue') && data.continue.hasOwnProperty('clcontinue')) {
			params.continue = 'clcontinue||';
			params.clcontinue = data.continue.clcontinue;
		} else if (iwarticlesRemaining.length === 0) {
			break; // Exit if there are no more articles to process
		}
	}

	return targetCats;
}


module.exports = router;
const express = require('express')

const { mwn } = require('mwn');

var init = require('../static/init.js');
const router = express.Router()

const naslov = "Hunspell";

router.get("/", async (req, res) => {

	let rand = req.query.rand || '';
	let page = req.query.page || '';

	try {
		await generateResultPage(req, res, rand, page);
	}
	catch (err) {
		init.failureCallback(err, req, res);
	}
	res.end()
});



async function generateResultPage(req, res, rand, page, lang = "hr") {
	const startMem = process.memoryUsage().rss / 1024 / 1024; //MB
	const startTime = process.hrtime();

	apiUrl = 'https://' + lang + '.wikipedia.org';

	// bot account and database access credentials, if needed
	const crendentials = require('../../credentials.json');

	const client = await mwn.init({
		apiUrl: apiUrl + '/w/api.php',
		// Can be skipped if the bot doesn't need to sign in    
		username: crendentials.bot_username,
		password: crendentials.bot_password,

		// Set your user agent (required for WMF wikis, see https://meta.wikimedia.org/wiki/User-Agent_policy):    
		userAgent: init.userAgent,
		// Set default parameters to be sent to be included in every API request    
		defaultParams: {
			assert: 'user' // ensure we're logged in    
		}
	});

	// need to do either a .getSiteInfo() or .login() before we can use the client object  
	await client.getSiteInfo();
	//await client.login();

	const fs = require('fs');
	const path = require('path');
	const Nodehun = require('nodehun');

	const affFile = fs.readFileSync('static/hr_HR.aff');
	const dicFile = fs.readFileSync('static/hr_HR.dic');

	const dictionary = new Nodehun(affFile, dicFile);

	// Initialize caches
	const spellCache = new Map();
	const suggestCache = new Map();

	console.log('/hunspell ' + (page ? 'page: ' + page : (rand ? '- random' : '- rc (default)')));

	res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
	res.write(init.generateHead(naslov));

	res.write(init.nav);

	res.write('<div class="container" style="padding-top: 10px;">');
	if (page) {
		res.write('Alat provjerava pravopis unesenog članka putem Hunspell biblioteke za hrvatski jezik.<br>')
		res.write(`<a href="/hunspell?rand=1">Provjeri pravopis nasumičnih članaka</a><br>`)
		res.write(`<a href="/hunspell">Provjeri pravopis izmjena u NP</a>`)
	} else {
		if (!rand) {
			res.write('Alat popisuje 10 najnovijih članaka u nedavnim promjenama i provjerava im pravopis putem Hunspell biblioteke za hrvatski jezik.<br>')
			res.write(`<a href="/hunspell?rand=1">Provjeri pravopis nasumičnih članaka</a>`)
		} else {
			res.write('Alat popisuje 10 nasumičnih članaka i provjerava im pravopis putem Hunspell biblioteke za hrvatski jezik.<br>')
			res.write(`<a href="/hunspell">Provjeri pravopis izmjena u NP</a>`)
		}
	}
	res.write('<div style="text-align: center;">Posebne zahvale autorima hrvatskog rječnika na <a href="https://github.com/krunose/hunspell-hr">GitHubu</a>: @krunose, Denis Lacković (<a href="http://cvs.linux.hr/spell">cvs.linux.hr/spell</a>), Mirko Kos, Boris Jurić i ostali.</div>')

	try {

		const lista = await dohvatiPopisISadrzaj(10, client, rand, page);

		let typoArticleCount = 0;
		for (const e of lista) {

			let content = e.content
				.replace(/(\\n)*== ?izvori ?==[\s\S]*/gi, '')  // strip the end
				.replaceAll('\n\n', '\n')
				.replaceAll(/(=+[a-ž()\- ]+?=+)/gi, '<br>$1<br>')

			let typosFound = await parsePageContent(spellCache, suggestCache, dictionary, typoArticleCount, e.title, content, lang);

			res.write(typosFound.join(''));



			res.write('<hr />')
			typoArticleCount++;
		}

		const endTime = process.hrtime(startTime);
		const elapsedTimeInSeconds = (endTime[0] + endTime[1] / 1e9);
		const memoryUsage = (process.memoryUsage().rss / 1024 / 1024) - startMem;


		res.write(init.getTranslation(lang, 'p-final-time', elapsedTimeInSeconds.toFixed(3), memoryUsage.toFixed(3),));

		res.write('</div>');

	} catch (error) {
		console.log(error)
	}
}

async function parsePageContent(spellCache, suggestCache, dictionary, index, title, content, lang) {
	const out = [`${index + 1}: <a href="https://${lang}.wikipedia.org/wiki/${title.replaceAll(' ', '_')}">${title}</a><br />`];

	const punctRegex = /[.,=()\"\'\&;:]/g;
	const escapedWordRegex = /[.*+?^${}()|[\]\\]/g;
	const newlineRegex = /\n+/;
	const spaceRegex = /\s+/;

	const sentences = content.split(newlineRegex);
	const processedSentences = new Array(sentences.length);

	for (let i = 0; i < sentences.length; i++) {
		let sentence = sentences[i];
		let processedSentence = sentence;
		const words = sentence
			.replace(punctRegex, '')
			.trim()
			.split(spaceRegex)
			.filter(Boolean);

		if (words.length === 0) {
			processedSentences[i] = sentence + ' ';
			continue;
		}

		const seenWords = new Set();

		for (const word of words) {
			if (seenWords.has(word) || word === "-" || word === "br") continue;
			seenWords.add(word);

			let isCorrect = spellCache.get(word);
			if (isCorrect === undefined) {
				isCorrect = await dictionary.spell(word);
				spellCache.set(word, isCorrect);
			}

			if (!isCorrect) {
				let suggestions = suggestCache.get(word);
				if (suggestions === undefined) {
					suggestions = await dictionary.suggest(word);
					suggestCache.set(word, suggestions);
				}

				const abbrReplacement = `<abbr style='background:yellow;' title='${suggestions.join(', ')}'>${word}</abbr>`;
				const escapedWord = word.replace(escapedWordRegex, '\\$&');
				const wordRegex = new RegExp(`\\b${escapedWord}\\b`, 'g');
				processedSentence = processedSentence.replace(wordRegex, abbrReplacement);
			}
		}
		processedSentences[i] = processedSentence + ' ';
	}
	out.push(...processedSentences);

	return out;
}




async function dohvatiPopisISadrzaj(limit, client, rand, page) {
	let pageids = []
	let lista = [];
	let remaining = limit;
	let params = {}
	if (page) {
		params = {
			"action": "query",
			"format": "json",
			"prop": "extracts",
			"titles": page,
			"formatversion": "2",
			"explaintext": 1
		}
	} else {

		if (rand) {
			console.log('random')
			params = {
				"action": "query",
				"format": "json",
				"prop": "extracts",
				"generator": "random",
				"formatversion": "2",
				"explaintext": 1,
				"grnnamespace": "0",
				"grnfilterredir": "nonredirects",
				"grnlimit": "1"
			}
		} else {
			console.log('RC')
			params = {
				"action": "query",
				"format": "json",
				"prop": "extracts",
				"continue": "grncontinue||",
				"generator": "recentchanges",
				"formatversion": "2",
				"explaintext": 1,
				"grcdir": "newer",
				"grcnamespace": "0",
				"grclimit": "1"
			}
		}
	}

	while (remaining > 0) {

		let data = await client.request(params);

		let pagesRetrieved = 0;
		data.query.pages.forEach((e) => {
			if (e.hasOwnProperty('extract') && !(pageids.includes(e.pageid))) {
				lista.push({
					title: e.title,
					content: init.escapeSpecials(e.extract)
				});
				pagesRetrieved++;
				pageids.push(e.pageid)
			}
		});

		remaining -= pagesRetrieved;

		// set new params
		delete params.grccontinue;
		delete params.rvcontinue;


		if (!data.hasOwnProperty('continue')) {
			break;
		} else { // more than 12 MB
			if (data.continue.hasOwnProperty('rvcontinue')) {
				params.rvcontinue = data.continue.rvcontinue;
			} else if (data.continue.hasOwnProperty('grccontinue')) {
				params.grccontinue = data.continue.grccontinue;
			}
		}
	}
	console.log('Total results retrieved: ' + lista.length);

	return lista;
}

module.exports = router;
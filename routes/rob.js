const express = require('express')

const { mwn } = require('mwn');
const { default: title } = require('mwn/build/title');

var init = require('../static/init.js');
const { template: t } = require('mwn/build/static_utils.js');

const router = express.Router()

const naslov = "Ažuriranje ROB";

router.get("/", async (req, res) => {

	try {
		await generateResultPage(req, res);
	}
	catch (err) {
		init.failureCallback(err, req, res);
	}
	res.end()
});



async function generateResultPage(req, res, lang = "hr") {
	const startMem = process.memoryUsage().rss / 1024 / 1024; //MB
	const startTime = process.hrtime();

	apiUrl = 'https://' + lang + '.wikipedia.org';

	// bot account and database access credentials, if needed
	const crendentials = require('../../credentials.json');

	const client = await mwn.init({
		apiUrl: apiUrl + '/w/api.php',
		// Can be skipped if the bot doesn't need to sign in    
		username: crendentials.bot_username,
		password: crendentials.bot_password,

		// Set your user agent (required for WMF wikis, see https://meta.wikimedia.org/wiki/User-Agent_policy):    
		userAgent: init.userAgent,
		// Set default parameters to be sent to be included in every API request    
		defaultParams: {
			assert: 'user' // ensure we're logged in    
		}
	});

	// need to do either a .getSiteInfo() or .login() before we can use the client object  
	await client.getSiteInfo();
	//await client.login();

	console.log('/rob');

	res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
	res.write(init.generateHead(naslov));

	res.write(init.nav);

	res.write('<div class="container" style="padding-top: 10px;">');

	let months = ["0", "Siječanj", "Veljača", "Ožujak", "Travanj", "Svibanj", "Lipanj", "Srpanj", "Kolovoz", "Rujan", "Listopad", "Studeni", "Prosinac"];
	const now = new Date()
	const currentMonth = now.getMonth() + 1;
	const currentYear = now.getFullYear();

	fullContent = [];

	// Iterate over years
	for (let year = 2021; year <= currentYear; year++) {
		// Determine the starting month for the first year
		let startMonth = (year === 2021) ? 12 : 1;
		// Determine the ending month for the current year
		let endMonth = (year === currentYear) ? currentMonth : 12;

		let titles = []

		// Iterate over months
		for (let month = startMonth; month <= endMonth; month++) {

			titles.push(`Wikipedija:Rasprava o brisanju/Rasprave/${months[month]} ${year}.`);
		}
		fullContent.push({
			year: year,
			months: await getRobContent(client, titles.join('|'))
		});
	}

	let json = await parseContenttoJSON(fullContent, client);
	//console.log(luaTable)
	/*res.write('<textarea rows=20 cols=1000 readonly>')
	res.write(JSON.stringify(json, null, 2));
	res.write('</textarea>')
	*/
	let updated = `${currentMonth} ${currentYear}`
	let luaTable = await jsonToLua(json, updated);
	res.write("Ažuriranje teksta za <a href='https://hr.wikipedia.org/wiki/Modul:Rasprava_o_brisanju/data'>listu brisanja</a><br>");
	/*	res.write('<textarea rows=20 cols=100 readonly>')
		res.write(luaTable);
		res.write('</textarea>')
	*/

	const he = require('he');

	// Decode any encoded entities before saving
	luaTable = he.decode(luaTable);

	var success = await client.save('Modul:Rasprava_o_brisanju/data', luaTable, 'Bot: ažuriranje');
	if (success.nochange) {
		res.write(init.colorText('red', 'Tekst liste brisanja već je ažuran') + `. (<a href="https://hr.wikipedia.org/w/index.php?title=Modul%3ARasprava_o_brisanju%2Fdata&diff=curid">zadnji diff</a>)`)
	} else {
		if (success.result == "Success") {
			res.write(init.colorText('green', 'Tekst liste brisanja je uspješno ažuriran') + `. (<a href="https://hr.wikipedia.org/wiki/Special:Diff/${success.newrevid}">novi diff</a>)`)
		}
	}

	if (!success.nochange) {
		let purgeStatus = await client.purge(["Modul:Rasprava o brisanju/data/doc", "Predložak:Kafić/Aktivne rasprave"], { forcerecursivelinkupdate: true })

		purgeStatus.forEach((item) => {
			if (item.purged) {
				res.write(`<br />Stranica <a href="https://hr.wikipedia.org/wiki/${item.title.replace(' ', '_')}">${item.title}</a> ` + init.colorText('green', 'uspješno je osvježena', 0) + '.');
			} else {
				res.write(`<br />Stranica <a href="https://hr.wikipedia.org/wiki/${item.title.replace(' ', '_')}">${item.title}</a> ` + init.colorText('red', 'nije uspješno osvježena', 0) + '.');
			}
		});
	}
}


async function getRobContent(client, titles) {
	//console.log(titles);
	let yearlyContent = [];
	await client.request({
		"action": "query",
		"format": "json",
		"prop": "revisions",
		"titles": titles,
		"redirects": 1,
		"formatversion": "2",
		"rvprop": "content",
		"rvslots": "main"
	}).then(async function (data) {

		data.query.pages.forEach(e => {
			let title = e.title.split('/')[2];
			if (!e.hasOwnProperty('missing')) {
				content = e.revisions[0].slots.main.content;
				yearlyContent.push({
					title: title,
					content: content
				})
			}

		});
	});

	return yearlyContent
}

function parseContenttoJSON(data, client) {
	let json = {};

	data.forEach(yearData => {
		yearData.months.forEach(async monthData => {
			const mjesec = monthData.title;
			const text = monthData.content;
			if (mjesec) {

				let templates = await new client.wikitext(text).parseTemplates()
					.filter(tpl => tpl.name.toLowerCase() == 'rasprava o brisanju');
				for (const t of templates) {
					// console.log(t.parameters)
					var ime = t.getParam('ime_stranice')?.value.trim()
					var status = t.getParam('status')?.value.trim()

					if (!json[ime]) {
						json[ime] = []; // Initialize array for the title if not exists
					}
					json[ime].push({ // Push new object into the title array
						mjesec: mjesec,
						status: status
					});
				}

			}
		});
	});

	return json;
}

function jsonToLua(data, updated) {
	let luaTable = `local rob = {\n`;
	luaTable += `\t['@UPDATED@'] = '${updated}',`;

	// Convert JSON object to array of entries
	let dataArray = Object.entries(data);

	for (let i = 0; i < dataArray.length; i++) {
		let [key, value] = dataArray[i];

		//if (key.match(/&#/)) console.error(key)

		let escKey = key
			.replaceAll(/''/g, "&#39;&#39;")
			.replaceAll(/&/g, "&amp;")
			.replaceAll(":Kategorija:", "Kategorija:")



		//if (key.match(/&#/)) console.error('      ' + escKey)

		luaTable += `\n\t['${escKey}'] = { `; // Start of inner table

		// Iterate over each object in the array
		for (let j = 0; j < value.length; j++) {
			luaTable += `{ mjesec = "${value[j].mjesec}", status = "${value[j].status}" }, `;
		}
		luaTable += `},`; // End of inner table
	}

	luaTable += "\n}\nreturn rob";

	return luaTable;
}


module.exports = router;
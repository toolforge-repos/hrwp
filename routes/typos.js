const express = require('express')

const { mwn } = require('mwn');
const { default: title } = require('mwn/build/title');

var init = require('../static/init.js');
const { forEach } = require('xregexp');

const router = express.Router()

// async!!!
router.get("/:lang", async (req, res) => {

	if (req.params.lang) {

		var lang = req.params.lang;

		if (!init.wikiLangs.includes(lang)) {
			console.log('/typos - lang fail: ' + lang);
			// krivo ime
			res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
			res.write(init.generateErrorPage('Typos', 'Unsupported language.'));
			res.end();
			return;
		}

		await generateCheckPage(req, res, lang, 100);
	}


	res.end();
});


async function generateCheckPage(req, res, lang, limit) {
	apiUrl = 'https://' + lang + '.wikipedia.org';

	// bot account and database access credentials, if needed
	const crendentials = require('../../credentials.json');

	const strings = require("../static/strings.json");

	const client = await mwn.init({
		apiUrl: apiUrl + '/w/api.php',
		// Can be skipped if the bot doesn't need to sign in    
		username: crendentials.bot_username,
		password: crendentials.bot_password,

		// Set your user agent (required for WMF wikis, see https://meta.wikimedia.org/wiki/User-Agent_policy):    
		userAgent: init.userAgent,
		// Set default parameters to be sent to be included in every API request    
		defaultParams: {
			assert: 'user' // ensure we're logged in    
		}
	});

	// need to do either a .getSiteInfo() or .login() before we can use the client object  
	await client.getSiteInfo();
	//await client.login();

	console.log('/typos: ' + lang + '; ' + limit);

	res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
	res.write(init.generateHead('Provjera pravopisa'));

	res.write(init.nav);
	res.write('<div class="container" style="padding-top: 10px;">')

	let typos = await getListOfTypos(req, res, client);
	if (typos.length == 0) {
		res.write('Project ' + lang + 'wiki ' + init.colorText('red', 'has no typos listed') + '. Aborting...');
		init.chooser(res, 'typos');
		return 0;
	}

	//used in checkTypos(), if regex parsing error
	res.write(`<style>
	.regex-error {
		padding: 0 10px; 
	}
	.error-explainer {
		margin-left: 10px;
	}</style>`);

	res.write(init.mnozina(typos.length, 'Dohvaćen @ ispravak', 'Dohvaćena @ ispravka', 'Dohvaćeno @ ispravaka') + '. ');
	res.write('Prijedloge novih ispravaka ostaviti <a href="https://' + lang + '.wikipedia.org/wiki/Wikipedia_talk:AutoWikiBrowser/Typos">ovdje</a>.</br />');

	res.write('<b>Legenda: </b>');
	res.write('Tekst neposredno prije. [<abbr style="background-color: yellow;" class="typo" title="ID: [broj unosa] - [pregled poklapanja]">greška</abbr>]'
		+ ' (<abbr style="background-color: lightblue;" class="typo" title="ID: [broj ispravka] - pregled poklapanja]">ispravak</abbr>)'
		+ ' Tekst neposredno poslije.');

	const lista = await dohvatiPopisISadrzaj(limit, client);

	res.write('<br />');
	res.write(init.mnozina(lista.length, 'Provjeren @ članak', 'Provjerena @ članka', 'Provjereno @ članaka') + ', ');
	res.write('od kojih sljedeći imaju pravopisne greške:<br /><br />');

	let typoArticleCount = 0;
	for (const e of lista) {
		let typosFound = await parsePageContent(req, res, typoArticleCount, e.title, e.content, typos, lang);

		if (typosFound) {
			typoArticleCount++;
			res.write('<hr />')
		}
	}

	console.log(typoArticleCount + ' ');
	if (typoArticleCount == 0) {
		res.write(init.colorText('green', 'Svi su članci pravopisno ispravni!', 1));
	}

	init.chooser(res, 'typos');

	res.write('</div>'); // container

}

module.exports = router;



async function dohvatiPopisISadrzaj(limit, client) {
	let lista = [];
	let remaining = limit;
	let params = {
		"action": "query",
		"format": "json",
		"prop": "revisions",
		"generator": "recentchanges",
		"redirects": "1",
		"formatversion": "2",
		"rvprop": "content",
		"rvslots": "main",
		"grcdir": "newer",
		"grctype": "edit|new",
		"grcnamespace": "0",
		"grctoponly": "1",
	}

	while (remaining > 0) {
		params.grclimit = Math.min(500, remaining);

		let data = await client.request(params);

		let pagesRetrieved = 0;
		data.query.pages.forEach((e) => {
			if (e.hasOwnProperty('revisions')) {
				lista.push({
					title: e.title,
					content: init.escapeSpecials(e.revisions[0].slots.main.content)
				});
				pagesRetrieved++;
			}
		});

		remaining -= pagesRetrieved;
		console.log('Total results retrieved: ' + lista.length);

		// set new params
		delete params.grccontinue;
		delete params.rvcontinue;


		if (!data.hasOwnProperty('continue')) {
			break;
		} else { // more than 12 MB
			if (data.continue.hasOwnProperty('rvcontinue')) {
				params.rvcontinue = data.continue.rvcontinue;
			} else if (data.continue.hasOwnProperty('grccontinue')) {
				params.grccontinue = data.continue.grccontinue;
			}
		}
	}

	return lista;
}


async function getListOfTypos(req, res, client) {
	let typos = [];
	await client.request({
		"action": "query",
		"format": "json",
		"prop": "revisions",
		"titles": "Wikipedia:AutoWikiBrowser/Typos",
		"formatversion": "2",
		"rvprop": "content",
		"rvslots": "main",
		"rvlimit": "1",
		"rvcontentformat-main": "text/x-wiki"
	}).then(async function (data) {
		// if typos page doesn't exist
		if (data.query.pages[0].hasOwnProperty('missing')) {
			return [];
		}
		let content = data.query.pages[0].revisions[0].slots.main.content;

		// exclude any lines tarting with a comment
		// Split the multiline string into an array of lines
		const lines = content.split('\n');

		// Filter out lines starting with <!--
		var filteredLines = lines.filter(line => !line.trim().startsWith('<!--'));
		filteredLines = filteredLines.filter(line => !line.trim().includes('disabled='));
		filteredLines = filteredLines.filter(line => !line.trim().includes('ISBN'));

		// Join the remaining lines back into a string
		content = filteredLines.join('\n');

		// regex
		const regex = /find="([\S\s]+?)" replace="([\S\s]+?)"/gi;

		// Use the exec method in a loop to extract matches
		let matches;

		while ((matches = regex.exec(content)) !== null) {
			// matches[1] contains the value inside the capturing group
			typos.push({
				find: matches[1]
					//.replaceAll(/\\b/g, "\\W")
					//.replace('//', '/').replace(/\\\\/g, "\\")
					//.replace(/\b/g, "\\b(?![\\p{M}])")
					.replace(/\\b/g, '\\b(?:\\p{Mark}|^|$)')
				,
				replace: matches[2]
					//.replaceAll(/\\b/g, "\\W")
					//.replace('//', '/').replace(/\\\\/g, "\\")
					//.replace(/\b/g, "\\b(?![\\p{M}])")
					.replace(/\\b/g, '\\b(?:\\p{Mark}|^|$)')
			});
		}
	});

	// diacritics are considered a word boundary 
	/*
	const modifiedTypos = typos.map(str =>
		str.replace(/\\b/g, '(?:^|(?<=\\W))')
	  );
	  
	return modifiedTypos;
	*/
	console.log('Typos fetched');
	return typos;
}



async function parsePageContent(req, res, index, title, content, typos, lang) {

	const wtf = require('wtf_wikipedia');

	let pageText = wtf(content).text();

	const sentences = pageText.split('\n');

	// Flag to check if any typos were found
	let typosFound = false;
	let output = '';

	sentences.forEach(sentence => {
		const highlightedSentence = checkTypos(req, res, sentence, typos);

		// Check if any matches were found
		if (highlightedSentence !== null) {
			// Set the flag to true if at least one typo is found
			typosFound = true;
			output += highlightedSentence;
		}
	});

	// Output the default message if no typos are found
	if (typosFound) {
		res.write((index + 1) + ': <a href="https://' + lang + '.wikipedia.org/wiki/' + title.replaceAll(' ', '_') + '">' + title + '</a><br />');
		res.write(output);
		return true;
	}
	return false;
}

function checkTypos(req, res, sentence, typos) {
	// Variable to store the highlighted sentence
	let highlightedSentence = sentence;

	// Flag to check if any replacements were made
	let replacementsMade = false;

	// Iterate through each typo object in the array
	for (const [index, typo] of typos.entries()) {
		let regex;
		try {
			// Create a regular expression with the find property and the global flag for multiple matches
			regex = new RegExp(typo.find, 'g');
		} catch (error) {
            // Handle the exception (e.g., log the error, skip the current typo, etc.)
            res.write(init.colorText('red', 'Error while processing typo') + ' <code class="regex-error">' + init.escapeSpecials(typo.find) + '</code> ==> <code class="regex-error">' + init.escapeSpecials(typo.replace) + '</code><br />' 
			 + '<span class="error-explainer"> > '+ init.escapeSpecials(error.message) + '</span><br />');

            // Remove the faulty typo from the array
            typos.splice(index, 1);

            // Continue to the next iteration
            continue;
        }

		// Use replace with a callback function to handle placeholders in the replace property
		highlightedSentence = highlightedSentence.replace(regex, function () {
			// The first argument (arguments[0]) is the entire match, and subsequent arguments are capturing groups
			const groups = Array.from(arguments).slice(1);

			// Use the captured groups in the replacement string
			const replacedText = typo.replace.replace(/\$(\d+)/g, function (match, groupIndex) {
				const groupNumber = parseInt(groupIndex, 10);
				return groups[groupNumber - 1] || ''; // Use the captured group or an empty string if not present
			});

			if (arguments[0] !== replacedText) {
				replacementsMade = true;
			}

			return `[<abbr style="background-color: yellow;" class="typo" title="ID: ${index} - ${typo.find}">${arguments[0]}</abbr>]
            (<abbr style="background-color: lightblue;" class="typo" title="ID: ${index} - ${typo.replace}">${replacedText}</abbr>)`;
		});
	};

	// Return the highlighted sentence or null if no match is found
	return replacementsMade ? highlightedSentence : null;
}





const express = require('express')

const { mwn } = require('mwn');
const { default: title } = require('mwn/build/title');

var init = require('../static/init.js');

var testURL = ["goo.gl", "t.co", "amzn.to"];

var url = ["0url.com", "1tiny.com", "6url.com", "babyurl.com", "goo.gl",
	"lin.kz", "link.toolbot.com", "linkezy.com", "linktrim.com", "makeashorterlink.com",
	"metamark.com", "minilien.com", "notlong.com", "qurl.net", "shorl.com", "shurl.org",
	"smcurl.com", "snipurl.com", "tinyclick.com", "tinyurl.com", "url.etusivu.net",
	"url123.com", "urlcut.com", "urljr.com", "v3.net"];

var urlExpanded = ["7.ly", "u.lu", "a.gg", "a.nf", "zi.pe", "lt.tl",
	"kl.am", "pd.am", "vl.am", "x.se", "ff.im", "sn.im", "nn.nf",
	"su.pr", "tr.my", "zi.ma", "di.fy", "ru.ly", "ow.ly", "a2a.me",
	"fon.gs", "cutt.ly", "big.ly", "bit.ly", "bit.do", "xrl.us",
	"yep.it", "n9.cl", "xr.com", "zz.gd", "gg.gg", "url.ca",
	"atu.ca", "url.ie", "b54.in", "888.hn", "ub0.cc", "tiny.cc",
	"klck.me", "t.co", "0.mk", "qr.net", "cli.gs", "bud.url",
	"s.coop", "amzn.to", "minu.me", "korta.nu", "doiop.com",
	"lxcurl.com", "clicky.me"]

const router = express.Router()

router.get("/:lang", async (req, res) => {

	if (req.params.lang) {

		var lang = req.params.lang;

		if (!init.wikiLangs.includes(lang)) {
			console.log('/url - lang fail: ' + lang);
			// krivo ime
			res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
			res.write(init.generateErrorPage('Shortened URL Checker', 'Unsupported language.'));
			res.end();
			return;
		}

		await generateList(req, res, lang);
	}

	res.end();
});


async function generateList(req, res, lang) {
	const linkstart = 'https://' + lang + '.wikipedia.org';

	// bot account and database access credentials, if needed
	const crendentials = require('../../credentials.json');

	const strings = require("../static/strings.json");

	const client = await mwn.init({
		apiUrl: 'https://' + lang + '.wikipedia.org' + '/w/api.php',  // login to local projects, fetch CentralAuth tokens for others
		// Can be skipped if the bot doesn't need to sign in    
		username: crendentials.bot_username,
		password: crendentials.bot_password,

		// Set your user agent (required for WMF wikis, see https://meta.wikimedia.org/wiki/User-Agent_policy):    
		userAgent: init.userAgent,
		// Set default parameters to be sent to be included in every API request    
		defaultParams: {
			assert: 'user' // ensure we're logged in    
		}
	});

	// need to do either a .getSiteInfo() or .login() before we can use the client object  
	await client.getSiteInfo();
	//await client.login();

	console.log('/url: ' + lang);


	res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
	res.write(init.generateHead('Skraćene poveznice'));

	res.write(init.nav);
	res.write(`<style>
	.ns-main { background-color: yellow; }
	</style>`);

	res.write('<div class="container" style="padding-top: 10px;">');
	res.write('Find shortened urls. Articles (ns:0) are <span class="ns-main">marked</span>.')
	res.write('<div class="accordion" id="accordionExample">')

	//await checkLinks(req, res, client, testURL, linkstart);
	await checkLinks(req, res, client, url, linkstart);
	await checkLinks(req, res, client, urlExpanded, linkstart);

	res.write('</div>');
	init.chooser(res, 'url');
	res.write('</div>');


};

async function checkLinks(req, res, client, list, linkstart) {

	//var token = await fetchCentralAuthToken(client);
	//console.log(token);


	let html = '<div id="accordion">'; // Start the accordion container

	for (const [index, url] of list.entries()) {
		await client.request({
			"action": "query",
			"format": "json",
			"list": "exturlusage",
			"formatversion": "2",
			"euquery": url,
			"eulimit": "100",
		}).then(async function (data) {
			//console.log(data);
			if (data.query.exturlusage.length > 0) {

				// Generate accordion card for each URL
				html += `<div class="accordion-item">
	<h2 class="accordion-header">
		<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
			data-bs-target="#collapse${index}" aria-expanded="true" aria-controls="collapse${index}">
			<span class="d-block w-100 text-center" style="font-size:1rem;">${url}</span>
		</button>
	</h2>
	<div id="collapse${index}" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
		<div class="accordion-body">`;

				html += '<ol>'
				for (const page of data.query.exturlusage) {
					// Add content to the accordion body (replace with your actual content)
					html += '<li ' + (page.ns == 0 ? 'class="ns-main"': '') + '>'
					+ '<a href="' + linkstart + '/wiki/' + page.title.replaceAll(' ', '_') + '">' +
						page.title + '</a>' + '</li>';
				}
				html += '</ol>'

				// Close the accordion body
				html += '</div>' // accordion-body
				html += '</div>' //collapse-index
				html += '</div>' // accordion-item
				
				// Send the generated HTML as the response
				
			} else {
				//res.write('<p>' + url + ' nije korišten.</p>')
			}
		});
	}
	res.write(html);
	res.write('</div>'); // id accordion
}

async function fetchCentralAuthToken(client) {
	let token = '';

	await client.request({
		"action": "centralauthtoken",
		"format": "json",
		"formatversion": "2"
	}).then(async function (data) {
		token = data.centralauthtoken.centralauthtoken;

	});


	return token
}


module.exports = router;